import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QDialog

    from ui.widgetEdit_pyqt import Ui_widgetEditDialog
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QDialog

    from ui.widgetEdit_pyside import Ui_widgetEditDialog




# Mostly sets up info for roll_text_edit
class widgetEditDialog(QDialog):
    def __init__(self, widget_name, widget_txt, widgets, parent=None):
        # if we are passed a parent, this window can be a sheet. Neat!
        QDialog.__init__(self, parent, Qt.Sheet)

        self.ui = Ui_widgetEditDialog()
        self.ui.setupUi(self)
        self.ui.widgetNameLabel.setText("Edit Your Widget: " + widget_name)
        self.ui.rollTextEdit._setup(widget_name, widget_txt, widgets, self.ui.rollValidLabel)
        self.ui.rollRevertButton.pressed.connect(self.ui.rollTextEdit.revert_roll)
        self.ui.rollRevertButton.setEnabled(False)

        self.ui.rollTextEdit.accepted.connect(self.emit_accept)
        self.ui.rollTextEdit.textChanged.connect(self.sync_revert_button)


    def emit_accept(self):
        roll_txt = str(self.ui.rollTextEdit.toPlainText()).strip('\r').strip('\n')

        if self.ui.rollTextEdit.validate_roll():
            self.ui.buttonBox.accepted.emit()
            return

        self.ui.rollTextEdit.revert_roll(roll_txt)


    def sync_revert_button(self):
        self.ui.rollRevertButton.setEnabled(not self.ui.rollTextEdit.validate_roll())


    def last_valid(self):
        return self.ui.rollTextEdit.last_valid

import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtWidgets import QDialog

    from ui.grouper_pyqt import Ui_grouperDialog
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QDialog

    from ui.grouper_pyside import Ui_grouperDialog




# Adds an identifier to the QToolButton so we know which line to remove when the Remove button is clicked
class groupDialog(QDialog):
    addGroup = Signal(list, str)

    def __init__(self, selected, parent=None):
        # if we are passed a parent, this window can be a sheet. Neat!
        QDialog.__init__(self, parent, Qt.Sheet)
        self.selected = selected

        self.ui = Ui_grouperDialog()
        self.ui.setupUi(self)
        self.accepted.connect(self.emit_add_group)
        self.show()


    def emit_add_group(self):
        self.addGroup[list, str].emit(self.selected, str(self.ui.groupEdit.text()))

import os, sys

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt, QRect
    from PyQt5.QtWidgets import QWidget, QGroupBox, QStyleOptionGroupBox, QStyle, QStylePainter
else:
    from PySide.QtCore import Signal, Qt, QRect
    from PySide.QtGui import QWidget, QGroupBox, QStyleOptionGroupBox, QStyle, QStylePainter




class dropGroupBox(QGroupBox):
    boxCollapseChanged = Signal(bool)

    def __init__(self, parent=None):
        QGroupBox.__init__(self, parent)
        self.m_collapsed = False
        self.click_pos = ''
        self.os_type = sys.platform
        if self.os_type == "darwin":
            # self.button_area = QRect(-5, 3, 16, 16)
            self.button_area = QRect(-3, 5, 9, 9)
        elif self.os_type.startswith("win"):
            self.button_area = QRect(-2, 1, 12, 12)
        else:  # os_type.startswith("linux"):
            self.button_area = QRect(5, 5, 16, 16)

        self.min_h = self.minimumHeight()
        self.max_h = self.maximumHeight()


    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            option = QStyleOptionGroupBox()
            self.initStyleOption(option)

            if self.button_area.contains(event.pos()):
                self.click_pos = event.pos()
                return

        QGroupBox.mousePressEvent(self, event)


    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton and self.click_pos == event.pos():
            self.setCollapse(not self.m_collapsed)


    def paintEvent(self, event):
        paint = QStylePainter(self)
        option = QStyleOptionGroupBox()

        self.initStyleOption(option)
        paint.drawComplexControl(QStyle.CC_GroupBox, option)
        option.rect = self.button_area

        prim = QStyle.PE_IndicatorArrowRight if self.m_collapsed else QStyle.PE_IndicatorArrowDown
        paint.drawPrimitive(prim, option)


    def setCollapse(self, collapse):
        self.m_collapsed = collapse

        if self.m_collapsed:
            self.min_h = self.minimumHeight()
            self.max_h = self.maximumHeight()
            self.setMinimumHeight(0)
            if self.os_type == "darwin":
                self.setMaximumHeight(24)
            else:
                self.setMaximumHeight(29)
        else:
            self.setMinimumHeight(self.min_h)
            self.setMaximumHeight(self.max_h)

        for widget in self.findChildren(QWidget):
            widget.setHidden(collapse)

        self.boxCollapseChanged[bool].emit(collapse)

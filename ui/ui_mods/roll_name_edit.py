from .line_drop_edit import LineDropEdit

from ui import drag_drop as dd




# Adds useful Drag and Drop functionality to the standard QLineEdit
# Pretty specific to wanting to change 2 QLineEdits at once
class RollNameEdit(LineDropEdit):
    def __init__(self, parent=None):
        LineDropEdit.__init__(self, parent)
        self.roll_edit_box = None


    def set_roll_edit_box(self, box):
        self.roll_edit_box = box


    def dropEvent(self, drop_event):
        drop = drop_event.mimeData()

        pos = self.cursorPositionAt(drop_event.pos())

        if drop.hasFormat(dd.DD_MIME_ROLLS):
            drop_event.acceptProposedAction()
            name_str = self.parse_dict(drop.rolls(), 'Roll Name', pos=pos)
            self.setText(name_str)
            roll_str = self.roll_edit_box.parse_dict(drop.rolls(), 'Roll',
                                                     base_txt=str(self.roll_edit_box.text()))
            self.roll_edit_box.setText(roll_str)

        else:
            LineDropEdit.dropEvent(self, drop_event)

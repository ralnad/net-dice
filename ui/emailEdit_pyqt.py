# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/emailEdit.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_emailEditDialog(object):
    def setupUi(self, emailEditDialog):
        emailEditDialog.setObjectName("emailEditDialog")
        emailEditDialog.setWindowModality(QtCore.Qt.WindowModal)
        emailEditDialog.resize(450, 400)
        emailEditDialog.setSizeGripEnabled(True)
        emailEditDialog.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(emailEditDialog)
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.emailSubjectLabel = QtWidgets.QLabel(emailEditDialog)
        self.emailSubjectLabel.setObjectName("emailSubjectLabel")
        self.horizontalLayout_2.addWidget(self.emailSubjectLabel)
        self.emailSubject = QtWidgets.QLineEdit(emailEditDialog)
        self.emailSubject.setObjectName("emailSubject")
        self.horizontalLayout_2.addWidget(self.emailSubject)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.sendToLabel = QtWidgets.QLabel(emailEditDialog)
        self.sendToLabel.setWordWrap(True)
        self.sendToLabel.setObjectName("sendToLabel")
        self.verticalLayout.addWidget(self.sendToLabel)
        self.emailTextEdit = QtWidgets.QPlainTextEdit(emailEditDialog)
        self.emailTextEdit.setMinimumSize(QtCore.QSize(0, 27))
        self.emailTextEdit.setObjectName("emailTextEdit")
        self.verticalLayout.addWidget(self.emailTextEdit)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(4)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.emailInfoLabel = QtWidgets.QLabel(emailEditDialog)
        self.emailInfoLabel.setScaledContents(True)
        self.emailInfoLabel.setWordWrap(True)
        self.emailInfoLabel.setObjectName("emailInfoLabel")
        self.horizontalLayout.addWidget(self.emailInfoLabel)
        self.passwordEdit = QtWidgets.QLineEdit(emailEditDialog)
        self.passwordEdit.setText("")
        self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordEdit.setObjectName("passwordEdit")
        self.horizontalLayout.addWidget(self.passwordEdit)
        self.sendButtonBox = QtWidgets.QDialogButtonBox(emailEditDialog)
        self.sendButtonBox.setMaximumSize(QtCore.QSize(180, 16777215))
        self.sendButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.sendButtonBox.setObjectName("sendButtonBox")
        self.horizontalLayout.addWidget(self.sendButtonBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.errorLabel = QtWidgets.QLabel(emailEditDialog)
        self.errorLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.errorLabel.setObjectName("errorLabel")
        self.verticalLayout.addWidget(self.errorLabel)
        self.emailSubjectLabel.setBuddy(self.emailSubject)
        self.emailInfoLabel.setBuddy(self.passwordEdit)

        self.retranslateUi(emailEditDialog)
        self.sendButtonBox.rejected.connect(emailEditDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(emailEditDialog)
        emailEditDialog.setTabOrder(self.emailTextEdit, self.passwordEdit)
        emailEditDialog.setTabOrder(self.passwordEdit, self.sendButtonBox)
        emailEditDialog.setTabOrder(self.sendButtonBox, self.emailSubject)

    def retranslateUi(self, emailEditDialog):
        _translate = QtCore.QCoreApplication.translate
        emailEditDialog.setWindowTitle(_translate("emailEditDialog", "Edit Your Widget"))
        self.emailSubjectLabel.setText(_translate("emailEditDialog", "Email Subject:"))
        self.emailSubject.setPlaceholderText(_translate("emailEditDialog", "NetDice Rolls"))
        self.sendToLabel.setText(_translate("emailEditDialog", "Sending to:"))
        self.emailInfoLabel.setText(_translate("emailEditDialog", "Password: "))
        self.errorLabel.setText(_translate("emailEditDialog", "For your security, NetDice does not save your password."))


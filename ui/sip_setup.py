'''
SIP is an intermediate layer between C++ and Python for PyQt
QVariant must be set to version 2, or there's a lot more overhead we'd have to deal with

Must have SIP enabled for PyQt. If you remove the QVariant line you'll have to manually
convert a lot of lines from QVariant to QString. Setting QString to v2 should 
eliminate the need for conversion between QString/python string
'''
        
def SIPsetup():
    import sip
        
    API_NAMES = ['QDate', 'QDateTime', 'QString', 'QTextStream', 'QTime', 'QUrl', 'QVariant']
    API_VERSION = 2

    for name in API_NAMES:
        sip.setapi(name, API_VERSION)
        
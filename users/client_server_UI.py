import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, QObject
    from PyQt5.QtWidgets import QActionGroup, QColorDialog
else:
    from PySide.QtCore import QObject, Signal
    from PySide.QtGui import QActionGroup, QColorDialog


from .user_base import user
import net_dice_globals as NDG

from threads.ip_thread import IPThread




# This class handles the interaction between the user classes,
# the UI, and the rest of the program.
class UserView(QObject):
    startAE = Signal()
    startClient = Signal()
    startServer = Signal()
    aeDisconnect = Signal()
    clientDisconnect = Signal()
    serverDisconnect = Signal()
    ConnTypeChanged = Signal(str)

    def __init__(self, UI, prefs_dict={}):
        logging.info("Creating the user ui")
        QObject.__init__(self)  # MUST init QObject if you descend from it
        self.ui = UI.ui  # UI stuff
        self.main_win = UI  # UI stuff, including the main window.
        # This is only used to resize the window based on the user mode, and could be better handled.

        self.user = user('')
        self.user.set_user_info(prefs_dict)

        self.ui_mode = None

        # server stuff
        self.server_external_IP = ''

        # When server mode is enabled, a thread is spawned to discover the server's external IP
        # python threads block when using Qt. Using Qt threads instead
        self.ip_thread = IPThread()

        temp_ui = self.set_user_info(prefs_dict)
        self._setup_ui(temp_ui)

        self._setup_connections()
        self.sync_ui_info()


    def _setup_connections(self):
        connGroup = QActionGroup(self.ui.menuModes)
        self.ui.userNameEdit.editingFinished.connect(self.set_user_name)
        self.ui.userConnectButton.clicked.connect(self._connect)
        self.ui.userColorButton.clicked.connect(self.set_user_color)
        self.ui.userRollPrivatelyCheck.stateChanged[int].connect(self._set_hidden_rolls)

        # user_base
        self.user.nameChanged.connect(self._sync_user_name)
        self.user.colorChanged.connect(self._set_button_color)

        # Local
        connGroup.addAction(self.ui.actionLocal_Mode)
        self.ui.actionLocal_Mode.triggered.connect(self.setup_local_mode)

        # Client
        connGroup.addAction(self.ui.actionClient_Mode)
        self.ui.actionClient_Mode.triggered.connect(self.setup_client_mode)
        self.ui.clientConnectToEdit.returnPressed.connect(self.client_set_IP)
        self.clientDisconnect.connect(self.client_disconnect)

        # Server
        connGroup.addAction(self.ui.actionServer_Mode)
        self.ui.actionServer_Mode.triggered.connect(self.setup_server_mode)
        self.ui.serverPortEdit.returnPressed.connect(self.server_set_port)
        self.ip_thread.IPDiscovered[str].connect(self.server_set_IP)
        self.ip_thread.threadError[str, Exception].connect(self._ip_error)
        self.serverDisconnect.connect(self.server_disconnect)

        # AppEngine
        # connGroup.addAction(self.ui.actionInternet_Mode)
        # self.ui.actionInternet_Mode.triggered.connect(self.setup_ae_mode)
        # self.ui.aeRoomEdit.returnPressed.connect(self.ae_set_room)
        # self.aeDisconnect.connect(self.ae_disconnect)

        # Email
        # connGroup.addAction(self.ui.actionE_Mail_Mode)
        # self.ui.actionE_Mail_Mode.triggered.connect(self.setup_email_mode)


    def _sync_UI(self, ui_mode):
        logging.info("set_user_info: from {old} to {new}".format(old=self.ui_mode, new=ui_mode))
        if ui_mode != self.ui_mode:
            old_ui_mode = self.ui_mode

            self.ui_mode = ui_mode

            if self.user.online:
                if old_ui_mode == NDG.CLIENT_MODE:
                    self.clientDisconnect.emit()
                elif old_ui_mode == NDG.AE_MODE:
                    self.aeDisconnect.emit()
                else:  # if old_ui_mode == NDG.SERVER_MODE:
                    self.serverDisconnect.emit()

            self.ConnTypeChanged[str].emit(ui_mode)


    def sync_UI(self):
        self.ConnTypeChanged[str].emit(self.ui_mode)
        if self.ui_mode == NDG.CLIENT_MODE:
            self.ui.actionClient_Mode.setChecked(True)
        elif self.ui_mode == NDG.SERVER_MODE:
            self.ui.actionServer_Mode.setChecked(True)
        elif self.ui_mode == NDG.LOCAL_MODE:
            self.ui.actionLocal_Mode.setChecked(True)
        elif self.ui_mode == NDG.EMAIL_MODE:
            self.ui.actionE_Mail_Mode.setChecked(True)
        elif self.ui_mode == NDG.AE_MODE:
            self.ui.actionInternet_Mode.setChecked(True)


    def _setup_ui(self, temp_UI, ui_only=False):
        if temp_UI == NDG.CLIENT_MODE or temp_UI is None:
            self.setup_client_mode(ui_only=ui_only)
        elif temp_UI == NDG.SERVER_MODE:
            self.setup_server_mode(ui_only=ui_only)
        elif temp_UI == NDG.EMAIL_MODE:
            self.setup_email_mode(ui_only=ui_only)
        elif temp_UI == NDG.AE_MODE:
            self.setup_ae_mode(ui_only=ui_only)
        else:
            self.setup_local_mode(ui_only=ui_only)

        self.ui.userNameEdit.setText(self.user.name)
        self.ui.userRollPrivatelyCheck.setChecked(self.user.hidden_rolls)

        self._set_button_color()


    def _convert_ui_from_local(self, previous_state):
        if previous_state == NDG.LOCAL_MODE:
                self.ui.userBox.show()
                new_geom = self.main_win.geometry()
                new_geom.setHeight(self.main_win.height() + self.ui.userBox.height())
                self.main_win.setGeometry(new_geom)


    def refresh_user_ui(self):
        # self.ui.lowerChatPane.setMaximumHeight(self.ui.connectedUsersBox.maximumHeight())
        self._setup_ui(self.ui_mode, ui_only=True)


    def _connect(self):
        new_name = self.ui.userNameEdit.text()
        if new_name != self.user.name:
            self.user._set_name(self.ui.userNameEdit.text(), True)

        if self.ui_mode == NDG.CLIENT_MODE:
            self.connect_to_server()
        elif self.ui_mode == NDG.SERVER_MODE:
            self.setup_server()
        elif self.ui_mode == NDG.AE_MODE:
            self.connect_to_ae_room()


    def _set_hidden_rolls(self, state):
        self.user._set_hidden_rolls(state)


    def set_user_name(self, emit=True):
        return self.user._set_name(self.ui.userNameEdit.text(), emit)


    def _sync_user_name(self):
        self.ui.userNameEdit.setText(self.user.name)


    def _set_name(self, new_name, emit=True):
        return self.user._set_name(new_name, emit)


    def set_user_color(self):
        logging.info("Setting the users color")
        # sets the user's color to what they selected in the dialog
        temp_color = QColorDialog.getColor(self.user.color)
        if temp_color.isValid():
            self.user._set_color(temp_color)
            self._set_button_color()


    def _set_button_color(self):
        logging.info("Setting the button color")
        # A little hack to make the color button be the user's selected color.
        color_str = "{red}, {green}, {blue}".format(red=self.user.color.red(), green=self.user.color.green(), blue=self.user.color.blue())
        self.ui.userColorButton.setStyleSheet("background-color:rgb({rgb})".format(rgb=color_str))


    def get_curr_user_info(self):
        logging.info("user info: " + str(self.user.get_user_info()))
        return self.user.get_user_info()


    def get_user_info(self):
        prefs_dict = {
            'UI Mode': self.ui_mode,
            'User Settings': self.user.get_user_info()}
        return prefs_dict


    def set_user_info(self, prefs_dict):
        logging.info("set_user_info")
        temp_ui = NDG.CLIENT_MODE
        if 'UI Mode' in prefs_dict:
            temp_ui = prefs_dict['UI Mode']
        if 'Server Settings' in prefs_dict:
            self.server_set_user_info(prefs_dict['Server Settings'])
        if 'Client Settings' in prefs_dict:
            self.client_set_user_info(prefs_dict['Client Settings'])
        if 'User Settings' in prefs_dict:
            self.user.set_user_info(prefs_dict['User Settings'])
        return temp_ui


    # --- slot helpers ---

    def client_started(self, dict):
        self.connected_to_server(dict)

    def server_started(self, dict):
        self.server_online(dict)


    def client_stopped(self):
        self.client_disconnect()

    def server_stopped(self):
        self.server_disconnect()


    def start_failed(self, fail_dict):
        self.ui.statusBar.showMessage('New connection failed: ' + fail_dict['Reason'])


    def _set_roller_ui_active_state(self, state=True):
        # used when in server/client mode and connected.
        ui = self.ui

        ui.rollsBox.setEnabled(state)
        ui.widgetListBox.setEnabled(state)

        ui.connectedUsersBox.setEnabled(state)
        ui.historyChatBox.setEnabled(state)


    def sync_ui_info(self):
        self.ui.clientConnectToEdit.setText(self.user.connection_IP)
        self.ui.serverPortEdit.setText(str(self.user.server_port))
        self.ui.aeRoomEdit.setText(str(self.user.ae_room_name))


    # -- LOCAL MODE -- #
    def setup_local_mode(self, triggered=False, ui_only=False):
        logging.info("setup_local_mode: " + str(ui_only))
        # When the user switches to client mode, this makes the necessary UI adjustments
        if self.ui_mode != NDG.LOCAL_MODE or ui_only:
            self.ui.userBox.hide()
            self.ui.lowerChatPane.hide()
            window_height_diff = self.ui.userBox.height()
            new_geom = self.main_win.geometry()
            new_geom.setHeight(self.main_win.height() - window_height_diff)
            self.main_win.setGeometry(new_geom)

        if self.ui_mode != NDG.LOCAL_MODE and not ui_only:
            self._set_roller_ui_active_state(True)
            self._sync_UI(NDG.LOCAL_MODE)


    # -- AE MODE -- #
    # def connect_to_ae_room(self):
    #     logging.info("connect_to_ae_room")
    #     if self.user.online:
    #         self.aeDisconnect.emit()
    #         return True

    #     self.set_user_name(False)
    #     connect = self.ae_set_room()
    #     if not connect:
    #         return False

    #     self.startAE.emit()


    # def connected_to_ae_room(self, server_info):
    #     self.user.online = True
    #     self.ui.aeRoomEdit.setReadOnly(True)
    #     self._set_roller_ui_active_state(True)
    #     if self.ui_mode == NDG.AE_MODE:
    #         self.ui.userConnectButton.setText("Disconnect")


    # def ae_set_room(self):
    #     self.ui.statusBar.clearMessage()
    #     try:
    #         test_name = str(self.ui.aeRoomEdit.text()).strip()
    #         self.user._set_ae_room(test_name)
    #         self.ui.statusBar.showMessage('Room name set')
    #     except:
    #         self.ui.statusBar.showMessage('Invalid room name')
    #         return False
    #     self.ui.aeRoomEdit.setText(self.user.ae_room_name)
    #     return True


    # def setup_ae_mode(self, triggered=False, ui_only=False):
    #     logging.info("setup_ae_mode" + str(ui_only))
    #     # When the user switches to client mode, this makes the necessary UI adjustments
    #     if self.ui_mode != NDG.AE_MODE or ui_only:
    #         prev_state = self.ui_mode
    #         self.ui.userBox.setTitle("Internet Mode Options")

    #         self.ui.userBox.blockSignals(True)
    #         self.ui.aeLayout.show()
    #         self.ui.serverLayout.hide()
    #         self.ui.clientLayout.hide()
    #         self.ui.userBox.blockSignals(False)

    #         self.ui.lowerChatPane.show()
    #         self.ui.emailBox.hide()
    #         self.ui.connectLayout.show()
    #         self.ui.kickUserButton.hide()
    #         # self.ui.muteUserButton.hide()
    #         self.ui.connectedUsersBox.show()
    #         self.ui.lowerChatPane.setMaximumHeight(self.ui.connectedUsersBox.maximumHeight())

    #         if self.user.online:
    #             self.ui.userConnectButton.setText("Disconnect")
    #         else:
    #             self.ui.userConnectButton.setText("Connect")

    #         self._convert_ui_from_local(prev_state)

    #     if self.ui_mode != NDG.AE_MODE and not ui_only:
    #         self._set_roller_ui_active_state(False)
    #         self._sync_UI(NDG.AE_MODE)


    # def ae_disconnect(self):
    #     logging.info("ae_disconnect")
    #     self.user.online = False
    #     self.ui.aeRoomEdit.setReadOnly(False)
    #     if self.ui_mode == NDG.AE_MODE:
    #         self.ui.userConnectButton.setText("Connect")

    #     if self.ui_mode in NDG.ONLINE_STATES:
    #         self._set_roller_ui_active_state(False)


    # -- CLIENT MODE -- #
    def connect_to_server(self):
        logging.info("connect_to_server")
        if self.user.online:
            self.clientDisconnect.emit()
            return True

        self.set_user_name(False)
        connect = self.client_set_IP()
        if not connect:
            return False

        self.startClient.emit()


    def connected_to_server(self, server_info):
        self.user.online = True
        self.ui.clientConnectToEdit.setReadOnly(True)
        self._set_roller_ui_active_state(True)
        if self.ui_mode in NDG.CLIENT_STATES:
            self.ui.userConnectButton.setText("Disconnect")


    def client_set_IP(self):
        self.ui.statusBar.clearMessage()
        try:
            test_IP = str(self.ui.clientConnectToEdit.text()).strip()
            assert test_IP != ''
            self.user.connection_IP = test_IP
            self.ui.statusBar.showMessage('Server IP set')
        except:
            self.ui.statusBar.showMessage('Invalid server IP')
            return False
        self.ui.clientConnectToEdit.setText(self.user.connection_IP)
        return True


    def setup_client_mode(self, triggered=False, ui_only=False):
        logging.info("setup_client_mode: " + str(ui_only))
        # When the user switches to client mode, this makes the necessary UI adjustments
        if self.ui_mode != NDG.CLIENT_MODE or ui_only:
            prev_state = self.ui_mode
            self.ui.userBox.setTitle("Client Mode Options")

            self.ui.userBox.blockSignals(True)
            self.ui.aeLayout.hide()
            self.ui.serverLayout.hide()
            self.ui.clientLayout.show()
            self.ui.userBox.blockSignals(False)

            self.ui.lowerChatPane.show()
            self.ui.emailBox.hide()
            self.ui.connectLayout.show()
            self.ui.kickUserButton.hide()
            # self.ui.muteUserButton.hide()
            self.ui.connectedUsersBox.show()
            self.ui.lowerChatPane.setMaximumHeight(self.ui.connectedUsersBox.maximumHeight())

            if self.user.online:
                self.ui.userConnectButton.setText("Disconnect")
            else:
                self.ui.userConnectButton.setText("Connect")

            self._convert_ui_from_local(prev_state)

        if self.ui_mode != NDG.CLIENT_MODE and not ui_only:
            self._set_roller_ui_active_state(False)
            self._sync_UI(NDG.CLIENT_MODE)


    def client_disconnect(self):
        logging.info("client_disconnect")
        # if self.user.online:
        self.user.online = False
        self.ui.clientConnectToEdit.setReadOnly(False)
        if self.ui_mode == NDG.CLIENT_MODE:
            self.ui.userConnectButton.setText("Connect")

        if self.ui_mode in NDG.ONLINE_STATES:
            self._set_roller_ui_active_state(False)


    def client_set_user_info(self, prefs_dict):
        # TODO: only here for backwards compatibility. remove function in a couple months.
        if 'Last Connection' in prefs_dict:
            self.user.connection_IP = prefs_dict['Last Connection']
        self.sync_ui_info()


    # -- SERVER MODE -- #
    def setup_server(self):
        logging.info("setup_server")
        self.set_user_name(False)
        if self.user.online:
            self.serverDisconnect.emit()
            return True

        connect = self.server_set_port()
        if not connect:
            return False

        self.startServer.emit()


    def server_online(self, server_info):
        logging.info("server_online")
        self.user.online = True
        self.ui.serverPortEdit.setReadOnly(True)
        self._set_roller_ui_active_state(True)
        if self.ui_mode == NDG.SERVER_MODE:
            self.ui.userConnectButton.setText("Stop Server")


    def server_disconnect(self):
        logging.info("server_disconnect")
        # if self.user.online:
        self.user.online = False
        self.ui.serverPortEdit.setReadOnly(False)
        self.ui.userConnectButton.setText("Start Server")
        if self.ui_mode in NDG.ONLINE_STATES:
            self._set_roller_ui_active_state(False)


    def setup_server_mode(self, triggered=False, ui_only=False):
        logging.info("setup_server_mode: ui_only={0}".format(ui_only))
        # When the user switches to server mode, this makes the necessary UI adjustments
        if self.ui_mode != NDG.SERVER_MODE or ui_only:
            prev_state = self.ui_mode

            self.ui.userBox.setTitle("Server Mode Options")
            self.ui.externalIPLabel.setText("Your IP: Discovering...")
            self.ui.serverPortEdit.setText(str(self.user.server_port))

            self.ui.userBox.blockSignals(True)
            self.ui.aeLayout.hide()
            self.ui.serverLayout.show()
            self.ui.clientLayout.hide()
            self.ui.userBox.blockSignals(False)

            self.ui.connectLayout.show()
            self.ui.kickUserButton.show()
            self.ui.muteUserButton.show()
            self.ui.lowerChatPane.show()
            self.ui.connectedUsersBox.show()
            self.ui.lowerChatPane.setMaximumHeight(self.ui.connectedUsersBox.maximumHeight())
            self.ui.emailBox.hide()

            if self.user.online:
                self.ui.userConnectButton.setText("Stop Server")
            else:
                self.ui.userConnectButton.setText("Start Server")

            self._convert_ui_from_local(prev_state)
            if self.server_external_IP:
                self.ui.externalIPLabel.setText("Your IP: " + self.server_external_IP)
            else:
                # spawn in new thread
                self.ip_thread.get_IP()

        if self.ui_mode != NDG.SERVER_MODE and not ui_only:
            self._set_roller_ui_active_state(False)
            self._sync_UI(NDG.SERVER_MODE)


    def _ip_error(self, where, err):
        msg = where + ": " + str(err)
        self.ui.statusBar.showMessage(msg)
        logging.error(msg)


    def server_set_user_info(self, prefs_dict):
        # TODO: only here for backwards compatibility. remove function in a couple months.
        if 'Server Port' in prefs_dict:
            self.user.server_port = prefs_dict['Server Port']
        # self.user.set_user_info(prefs_dict)
        self.sync_ui_info()


    def server_set_port(self):
        self.ui.statusBar.clearMessage()
        try:
            test_port = int(self.ui.serverPortEdit.text())
            assert 1024 < test_port <= 20000
            self.user.server_port = test_port
            self.ui.statusBar.showMessage('Server Port Set')
        except:
            self.ui.statusBar.showMessage('Invalid Server Port. Must be between 1024 and 20000.')
            return False
        self.ui.serverPortEdit.setText(str(self.user.server_port))
        return True


    def server_set_IP(self, new_IP):
        # Shows the IP discovered by the external thread
        IP_string = "Your IP: "
        self.server_external_IP = new_IP
        self.ui.externalIPLabel.setText(IP_string + new_IP)


    # -- EMAIL MODE -- #
    # def setup_email_mode(self, triggered=False, ui_only=False):
    #     logging.info("setup_email_mode" + str(ui_only))
    #     # When the user switches to client mode, this makes the necessary UI adjustments
    #     if self.ui_mode != NDG.EMAIL_MODE or ui_only:
    #         prev_state = self.ui_mode
    #         self.ui.aeLayout.hide()
    #         self.ui.serverLayout.hide()
    #         self.ui.clientLayout.hide()
    #         self.ui.connectLayout.hide()

    #         self.ui.lowerChatPane.show()
    #         self.ui.connectedUsersBox.hide()
    #         self.ui.emailBox.show()
    #         self.ui.lowerChatPane.setMaximumHeight(self.ui.emailBox.maximumHeight())
    #         self._convert_ui_from_local(prev_state)

    #     if not ui_only:
    #         self._set_roller_ui_active_state(True)
    #         self._sync_UI(NDG.EMAIL_MODE)

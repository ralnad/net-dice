import logging, os, random

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, QObject
    from PyQt5.QtGui import QColor
else:
    from PySide.QtCore import Signal, QObject
    from PySide.QtGui import QColor

from .default_names import roman_names as default_names



# The base User class, from which Client, Server, and Local are derived.
# Local is a slightly different case from the other two, since no user info is stored.
# It still handles mode changes though.
class user(QObject):
    nameChanged = Signal()
    colorChanged = Signal()
    aeRoomChanged = Signal()
    setHiddenRoll = Signal()

    def __init__(self, user_name, user_color=None, ip=None, port=None, room_name=None):
        logging.info("Creating the user space")
        QObject.__init__(self)  # MUST init QObject if you descend from it

        self.name = user_name
        self.color = user_color or QColor(150, 225, 253)
        self.hidden_rolls = False
        self.is_gm = False
        self.color_str = str(self.color.name())

        # not needed for all user types, but provided for convenience.
        # This way, we can pass one user around and not worry about having bad info
        self.online = False  # Just a way of knowing when to show the "connect" or "disconnect" buttons
        self.connection_IP = ip or 'localhost'
        self.server_port = port or 1234
        self.ae_room_name = room_name or ''


    def _set_name(self, name, emit=True):
        bad_name = False
        try:
            # if the user puts in a non-ascii name, this will fail and they'll be sorry
            test_name = str(name).strip()[:31]
            bad_chars = '\'\"# {}:[]()\\=~`<>'

            if test_name == '':
                bad_name = True
            else:
                for char in bad_chars:
                    if char in test_name:
                        bad_name = True
                        break
        except:
            bad_name = True

        if self.name == name or self.name == test_name:
            logging.info("No setting done, identical name passed in")
            return

        if bad_name:
            test_name = default_names[random.randint(0, len(default_names) - 1)]

        name_change = test_name != self.name
        logging.info("set_user_name: temp_name:" + test_name + " self.user_name: " + self.name)

        self.name = test_name

        if test_name and name_change and emit:
            self.nameChanged.emit()

        return test_name and name_change


    def _set_color(self, color):
        logging.info("Setting the users color")
        # sets the user's color to what they selected in the dialog
        if self.color == color:
            logging.info("No setting done, identical color passed in")
            return
        else:
            # print(self.color.name(), color.name())
            self.color = color
            self.color_str = str(self.color.name())
            self.colorChanged.emit()


    def get_color(self):
        return self.color_str


    def _set_hidden_rolls(self, state):
        logging.info("Setting state for hidden rolls: " + str(state))
        self.hidden_rolls = state > 0
        self.setHiddenRoll.emit()


    def _set_ae_room(self, new_room):
        assert new_room != ''
        if new_room != self.ae_room_name:
            self.ae_room_name = new_room
            self.aeRoomChanged.emit()


    def get_user_info(self):
        prefs_dict = {
            'Name': self.name,
            'Color': self.color_str,
            'Hide Rolls': bool(self.hidden_rolls),
            'Last Connection': self.connection_IP,
            'Server Port': self.server_port,
            'Room Name': self.ae_room_name
        }
        return prefs_dict


    def set_user_info(self, prefs_dict):
        if 'Name' in prefs_dict:
            self.name = prefs_dict['Name']
        if 'Color' in prefs_dict:
            self.color = QColor(prefs_dict['Color'])
            self.color_str = str(self.color.name())
        if 'Hide Rolls' in prefs_dict:
            self.hidden_rolls = prefs_dict['Hide Rolls']
        if 'Last Connection' in prefs_dict:
            self.connection_IP = prefs_dict['Last Connection']
        if 'Server Port' in prefs_dict:
            self.server_port = prefs_dict['Server Port']
        if 'Room Name' in prefs_dict:
            self.ae_room_name = prefs_dict['Room Name']

del /Q "net_dice_win.zip" "widgets.json" "prefs.pkl" "Logger.txt"
rmdir /S /Q "build" "dist\NetDice"
python "make_win_info.py" "net_dice_globals.py"
pyinstaller -y "net_dice_win.spec"

robocopy /S "dist" "NetDice"
7z a -tzip "net_dice_win.zip" "NetDice\NetDice\" "NetDice\NetDice.exe"
del "netdice_manifest.xml"
rmdir /S /Q "NetDice"

if "%1" == "dist" (
	:: tmp will be moved to actual win position by mac build script, ensuring synced build release
	copy "net_dice_win.zip" "..\..\Dropbox (Personal)\NetDice\net_dice_win_tmp.zip"
)

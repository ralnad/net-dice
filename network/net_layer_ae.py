import logging, os, time, traceback

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, QObject, QThread
else:
    from PySide.QtCore import Signal, QObject, QThread

from trees_and_lists.user_list import UserList
import net_dice_helpers as nd_helpers
import net_dice_globals as NDG

from .NetCodeAE import NetCodeAE




# two threads: StartReactor and something to read from the queue
class AELayer(QObject):
    aeStarted = Signal(dict)
    aeStopped = Signal(dict)
    aeStartFailed = Signal(dict)
    aeDisconnected = Signal()
    rollResultReceived = Signal(dict)
    chatReceived = Signal(dict)

    def __init__(self, ui, local_user, user_list):
        QObject.__init__(self)
        self.ui = ui

        self.local_user = local_user
        logging.info("init: " + str(self.local_user.name))

        # Set up the User List (only applicable in client/server modes)
        self.user_list = user_list

        self.reactor_thread = ConnectionThread(self.local_user, self.user_list.user_list)
        self.net_connection = False

        self._setup_connections()


    def _setup_connections(self):
        self.reactor_thread.messageReceived[dict].connect(self.received_message)

        self.ui.usersView.removeUsers[list].connect(self.kick_users)

        self.aeDisconnected.connect(self.user_list.clear)
        self.aeDisconnected.connect(self.ui.chatTreeView.model()._disconnected)


    # the 'start' functions could have more descriptive failure reasons, but this is sufficient to determine whose fault it is.
    def start_client(self):
        try:  # finding and preparing the IP and port
            room_name = self.local_user.ae_room_name

            status = self.reactor_thread.connect_to_server(room_name)

            if status:
                self.reactor_thread.listen_for_messages()
                self.net_connection = True
                self.aeStarted[dict].emit({})

                self._reset_user_model()
            else:
                self.aeStartFailed[dict].emit({'Reason':'NetCode failure'})
        except Exception as e:
            traceback.print_exc()
            self.aeStartFailed[dict].emit({'Reason':str(e)})


    def _reset_user_model(self):
        self.user_list.reset_user_model(self.local_user, use_server=False)
        self.reactor_thread.set_user_list(self.user_list.user_list)


    def _stop_thing(self):
        self.reactor_thread.stop_connection()
        self.user_list.clear()
        self.net_connection = False


    def disconnect_from_server(self):
        logging.info("disconnecting from server")
        self._stop_thing()
        self.aeDisconnected.emit()


    # --- User Info ---
    def kick_users(self, user_list):
        if self.local_user.name in user_list:
            user_list.remove(self.local_user.name)
        for user in user_list:
            self.reactor_thread.kick_user(user)



    # --- Roller Code ---
    def send_roll_to_server(self, roll_dict):
        # use sendRoll to send chats too
        send_dict = {'User Info':{'Name':self.local_user.name, 'Hide Rolls':self.local_user.hidden_rolls}}
        if 'Roll' in roll_dict:
            send_dict['Roll'] = roll_dict
        elif 'Chat' in roll_dict:
            send_dict['Chat'] = nd_helpers.chat_wrap(self.local_user.name) + roll_dict['Chat']
        # send to server
        logging.info("sending roll to server" + str(send_dict))
        self.reactor_thread.client_send_out(send_dict)


    def send_rolls_to_server(self, roll_dict):
        logging.info("sending rolls to server:")
        if 'Rolls' in roll_dict:
            for roll in roll_dict['Rolls']:
                send_dict = {'Roll':roll, 'User Info':{'Name':self.local_user.name, 'Hide Rolls':self.local_user.hidden_rolls}}
                self.reactor_thread.client_send_out(send_dict)


    def set_message_color(self, message):
        if message['User Info']['Name'] == self.local_user.name:
            message['User Info']['Color'] = self.local_user.color
        else:
            user_color = self.user_list.get_user_color(message['User Info']['Name'])
            if user_color:
                message['User Info']['Color'] = user_color
        return message


    # --- Decode Incoming Signals ---
    def received_message(self, msg_dict):
        # Client-side message handling. No error-checking here, that's all handled by NetCode and the server.
        logging.info('received_message: ' + str(msg_dict))

        roll_dict = msg_dict['Result']

        if isinstance(roll_dict, dict):
            if 'New User' in roll_dict:
                # self.user_list.add_user(roll_dict['New User'])
                if 'Old User Name' in roll_dict:
                    self.user_list.change_user_name(roll_dict['Old User Name'], roll_dict['New User'])
                else:
                    self.reactor_thread.net.GetUsers()
            elif 'Roll' in roll_dict:
                roll_dict = self.set_message_color(roll_dict)
                self.rollResultReceived[dict].emit(roll_dict)
            elif 'Chat' in roll_dict:
                roll_dict = self.set_message_color(roll_dict)
                self.chatReceived[dict].emit(roll_dict)
            # elif 'GM' in roll_dict:
            #     self.user_list.set_gm(roll_dict['GM'])
            # This is to ensure status information for clients is updated correctly by identifying the user who disconnected
                '''elif 'Dead User' in roll_dict:
                self.user_list.remove_user(roll_dict['Dead User']['Name'])
                if roll_dict['Dead User']['Name'] == self.local_user.name:
                    self.ui.statusBar.showMessage("You got kicked! Reason: "+str(roll_dict['Dead User']['Reason']))
                    self.disconnect_from_server()
                '''
            elif 'New Color' in roll_dict:
                self.user_list.change_user_color(msg_dict['Is From'], roll_dict['New Color'])
            else:
                logging.info("received_message: unused input: " + str(roll_dict))
        elif isinstance(roll_dict, list):
            self.user_list.add_users(roll_dict)
        else:
            logging.info("received_message: malformed input")




from dice_roller import DiceRoller

class ConnectionThread(QThread):
    messageReceived = Signal(dict)

    def __init__(self, local_user, user_list, parent = None):
        QThread.__init__(self, parent)

        self.roller = DiceRoller()
        self.net = NetCodeAE(local_user)

        self.local_user = local_user
        self.old_name = self.local_user.name
        self.user_list = user_list
        self.exiting = False
        self.get_messages = False

        self._setup_connections()


    def _setup_connections(self):
        self.local_user.nameChanged.connect(self.update_name)
        self.local_user.colorChanged.connect(self.update_color)
        self.local_user.aeRoomChanged.connect(self.change_room)
        self.user_list.gmChanged.connect(self.update_gm)


    # --- Setup Stuff ---
    def set_user_list(self, user_list):
        self.user_list = user_list
        self._setup_connections()
        self.net.GetUsers()


    def connect_to_server(self, room_name):
        logging.info("connecting to AE: " + str(self.local_user.name) + ":" + str(room_name))
        # self.net.SetName(self.local_user.name)
        # self.net.SetColor(self.local_user.get_color())
        self.net.SetUser(self.local_user)
        status = self.net.StartClient(session=room_name)
        logging.info("AE status: " + str(status))
        if status:
            self.start()
            self._sync_name()
            return True
        return False


    def stop_connection(self):
        # TODO replace with kick code
        logging.info("AE: Stopping connection")
        cli = True
        if self.net.isClientRunning():
            cli = self.net.StopClient()
        #self.stop()
        self.get_messages = False
        return cli


    def update_name(self, force = False):
        # NetCode handles the duplicate names, so we don't need to duplicate that here
        logging.info("update_name: "+self.local_user.name+" "+self.old_name+" "+self.net.pyUser.name)
        if self.net.isClientRunning():
            # if force or self.local_user.name != self.net.pyUser.name:
            if force or self.local_user.name != self.old_name:
                self.net.SetName(self.local_user.name)
                #self.net.SetUser(self.local_user) #needed for syncing things up with the roller code
                self._sync_name()
        # else:
        #     self.old_name = self.local_user.name


    def _sync_name(self):
        if self.old_name != self.local_user.name:
            self.old_name = self.local_user.name
            if self.net.pyUser.name != self.local_user.name: #currently never happens
                self.local_user._set_name( self.net.pyUser.name, emit=False )


    def update_color(self):
        if self.net.isClientRunning():
            self.net.SetColor(self.local_user.get_color())
            print('set')


    def update_gm(self, new_gm_name):
        if self.net.isClientRunning():
            self.client_send_out({'Edit User':{'Name':new_gm_name, 'GM':True}})


    def change_room(self):
        if self.net.isClientRunning():
            new_room = self.local_user.ae_room_name
            self.user_list.clear_rows()
            self.net.SetSession(new_room)
            self.net.GetUsers()


    def client_send_out(self, send_dict):
        if self.net.isClientRunning():
            if 'Roll' in send_dict:
                if send_dict['User Info']['Hide Rolls']:
                    logging.info('RollGM')
                    self.net.RollGM(send_dict['Roll']['Roll Name'], send_dict['Roll']['Roll'], send_dict['Roll']['Roll Widgets'])
                else:
                    logging.info('Roll')
                    self.net.Roll(send_dict['Roll']['Roll Name'], send_dict['Roll']['Roll'], send_dict['Roll']['Roll Widgets'])
            else:
                self.net.ClientSend(send_dict)


    def kick_user(self, user_name, reason = "I felt like it"):
        if self.net.isServerRunning() and self.make_server and user_name and user_name != self.local_user.name:
            logging.info("Server Kicked "+user_name+" : "+str(reason))
            self.server_send_out({'Dead User':{'Name':user_name, 'Reason':reason}})


    def exit(self):
        self.stop_connection()
        self.exiting = True




    # --- Thread Controller Stuff ---
    def listen_for_messages(self):
        self.exiting = False
        self.get_messages = True


    def sort_messages(self):
        if not self.net.clientInbox.empty():
            inboxitem = self.net.clientInbox.get()
            self.messageReceived[dict].emit(inboxitem)


    def run(self):
        while not self.exiting:
            if self.get_messages:
                self.sort_messages()
            time.sleep(self.net.SLEEP_INCREMENT)


    def stop(self):
        self.get_messages = False
        self.exiting = True


    def __del__(self):
        self.exiting = True
        self.wait()

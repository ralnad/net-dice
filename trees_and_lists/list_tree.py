import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
else:
    from PySide.QtCore import Qt

from .base_tree_model import TreeModel
from .base_tree_view import TreeView



# Simplifies a TreeModel into a ListModel.
# Qt Supplies a ListModel, but this was easier to do since I have some experience with lists.
# Also, we can be a bit more flexible with some things this way.
class ListModel(TreeModel):
    def __init__(self, headers, editable=False, item_colors=False, parent=None):
        TreeModel.__init__(self, headers, editable, item_colors, parent)
        # self.setSupportedDragActions(Qt.CopyAction)

        if __QtLib__ != 'pyqt':
            self.setSupportedDragActions(Qt.CopyAction)


    if __QtLib__ == 'pyqt':
        def supportedDragActions(self):
            return Qt.CopyAction


    def add_item(self, item, fg_color=None, bg_color=None):
        # items should be a list of the cols to add to a single row
        if len(item) == self.rootItem.columnCount():
            return self.insert_row_to_end(item, fg_color=fg_color, bg_color=bg_color)
        return False


    def add_replace_item(self, item, fg_color=None, bg_color=None):
        success = False
        if len(item) == self.rootItem.columnCount():
            # find and replace a duplicate
            for row_num in range(self.rootItem.child_count()):
                row = self.rootItem.child(row_num)
                if row.data(0) == item[0]:
                    index = self.index(row_num, 0)
                    success = self.setRowData(index, item)
                    if success:
                        item = index.internalPointer()
                        item.set_background(bg_color)
                        item.set_foreground(fg_color)
                        return True
            if not success:
                return self.add_item(item, fg_color=fg_color, bg_color=bg_color)
        return False


    # Turn the list into a dictionary, with each value == {first_col:[other_cols]}
    # Is only used by Widget List, which optimizes the list a bit
    def get_list_dict(self):
        list_dict = {}
        for row_num in range(self.rootItem.child_count()):
            row_data = self.rootItem.child(row_num).all_data()
            list_dict[row_data[0]] = row_data[1:] or []
            if self.item_colors:
                list_dict[row_data[0]].append(self.bgColor)
        return list_dict


    def set_from_list_dict(self, list_dict):
        self.clear_rows()
        for key in list_dict:
            item = [key]
            item.extend(list_dict[key])
            success = self.insert_row_to_end(item)
            if not success:
                return False
        return True


    def flags(self, index):
        return Qt.ItemIsDropEnabled | Qt.ItemIsDragEnabled | TreeModel.flags(self, index)



# Just a TreeView class with a list optimization
class ListView(TreeView):
    def __init__(self, parent = None):
        TreeView.__init__(self, parent)

        self.setUniformRowHeights(True)

import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtGui import QColor
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QColor

from .base_tree_model import TreeSort
from .list_tree import ListModel

import net_dice_helpers as nd_helpers




class UserModel(ListModel):
    gmChanged = Signal(str)
    syncLocalGMPrivs = Signal(bool)

    def __init__(self, local_user, use_server=True, parent=None):
        headers=['User Name', 'Muted', 'GM']
        self.num_cols = 3
        if use_server:
            headers.append('Server')
            self.num_cols = 4
        self.name_col = 0
        self.muted_col = 1
        self.gm_col = 2
        self.server_col = 3
        ListModel.__init__(self, headers, editable=False, item_colors=True, parent=parent)

        self.local_user = local_user
        self.loud_color = "#000000"
        self.muted_color = QColor(150, 80, 80)  # others only
        self.hidden_color = QColor(80, 80, 80)  # local only
        self.old_name = local_user.name

        self.gm_name = ''
        self.server_name = ''


    def _get_user_row(self, name):
        for row_num in range(self.rootItem.child_count()):
            row = self.rootItem.child(row_num)
            if row.data(0) == name:
                return (row, row_num)
        return ('', -1)


    def _get_checked_row(self, col):
        for row_num in range(self.rootItem.child_count()):
            row = self.rootItem.child(row_num)
            if row.data(col) is True:
                return row
        return False


    def get_server_name(self):
        if self.server_name:
            return self.server_name

        if not self._server_on():  # we don't have a server column
            return False

        row = self._get_checked_row(self.server_col)
        if row:
            self.server_name = row.itemData[0]
            return row.itemData[0]
        return False


    def get_gm_name(self):
        if self.gm_name:
            return self.gm_name

        row = self._get_checked_row(self.gm_col)
        if row:
            self.gm_name = row.itemData[0]
            return row.itemData[0]
        return False


    def is_empty(self):
        return not self.rootItem.has_children()


    def _server_on(self):
        return self.num_cols > self.server_col


    def _row_updated(self, row_num):
        start_index = self.index(row_num, self.name_col)
        end_index = self.index(row_num, self.num_cols - 1)
        self.dataChanged.emit(start_index, end_index)


    def data(self, index, role):
        if not index.isValid():
            return None

        item = index.internalPointer()

        col = index.column()
        item_data = item.itemData[col]
        if col == self.muted_col:
            if role == Qt.DisplayRole:
                return ''
            elif role == Qt.CheckStateRole:
                return int(Qt.Checked if item_data else Qt.Unchecked)
        elif col == self.gm_col:
            # This column is pretty hackey, to accommodate the QItemDelegate.
            # We can't use unicode easily in the delegate, so in the event we're supposed
            # to show radio buttons, return numbers instead.
            if role == Qt.UserRole:
                # This encodes three options for the benefit of the QItemDelegate.
                # 0 means we have no gm privileges.
                # 1 and 2 mean we have privileges.
                # 1 also means this index is not a GM user.
                # 2 also means this index is a GM user.
                return int(2 if item_data else 1) if self.local_gm_privs() else False
            if self.local_gm_privs():
                if role == Qt.DisplayRole:
                    return ''
                    # return int( Qt.Checked if item_data else Qt.Unchecked )
                # No CheckStateRole in this instance, or the default painter will draw a checkbox.
                # Then we'll have both a checkbox and a radio button.
                # elif role == Qt.CheckStateRole:
                #    return int( Qt.Checked if item_data else Qt.Unchecked )
            if role == Qt.DisplayRole:
                return '\u2713' if item_data else ''
        elif col == self.server_col:
            if role == Qt.DisplayRole:
                return '\u2713' if item_data else ''

        return ListModel.data(self, index, role)


    def flags(self, index):
        if not index.isValid():
            return None

        col = index.column()

        if col == self.muted_col or col == self.gm_col and self.local_gm_privs():
            flags = Qt.ItemIsUserCheckable | Qt.ItemIsSelectable
            if not ((col == self.muted_col and self._user_name(index) == self.local_user.name) or (col == self.gm_col and index.data(role=Qt.CheckStateRole))):
                flags = flags | Qt.ItemIsEnabled  # can't mute yourself
        else:
            flags = ListModel.flags(self, index)

        return flags


    def setData(self, index, value, role=Qt.EditRole):
        col = index.column()
        if col == self.muted_col or col == self.gm_col and self.local_gm_privs():
            if role == Qt.EditRole:
                return False
            if role == Qt.CheckStateRole:
                item = self.getItem(index)
                item.itemData[col] = value
                if col == self.muted_col:
                    self._update_muted_color(index)
                elif col == self.gm_col:  # removing current user is prevented by flags
                    self.gmChanged[str].emit(str(index.sibling(index.row(), self.name_col).data()))
                self.dataChanged.emit(index, index)
                return True

        return ListModel.setData(self, index, value, role)


    def local_gm_privs(self):
        return self.local_user.name == self.get_server_name() or self.local_user.name == self.get_gm_name()


    def user_exists(self, user_name):
        (row, row_num) = self._get_user_row(user_name)
        return row_num > -1


    def get_user_dict(self):
        # Optimized get for user list.
        list_dict = {}
        for row_num in range(self.rootItem.child_count()):
            row = self.rootItem.child(row_num)
            row_data = row.all_data()
            list_dict[row_data[self.name_col]] = {'Color': str(row.bgColor.name())}
            if row_data[self.gm_col]:
                list_dict[row_data[self.name_col]]['GM'] = True
            if self._server_on() and row_data[self.server_col]:
                list_dict[row_data[self.name_col]]['Server'] = True
        return list_dict


    def add_item(self, item, fg_color=None, bg_color=None):
        # just making pretty colors before we hand it to the ListModel
        if bg_color and not fg_color:
            fg_color = self.get_font_color(bg_color)
        return ListModel.add_item(self, item, fg_color, bg_color)


    def add_replace_item(self, item, fg_color=None, bg_color=None):
        # just making pretty colors before we hand it to the ListModel
        if bg_color and not fg_color:
            fg_color = self.get_font_color(bg_color)
        return ListModel.add_replace_item(self, item, fg_color=fg_color, bg_color=bg_color)


    def get_user_color(self, user_name):
        (row, row_num) = self._get_user_row(user_name)
        if row:
            return row.bgColor
        return False


    def change_user_name(self, old_user, new_user):
        logging.info("UserModel: change_user_name: " + str(old_user) + ", " + str(new_user))

        (row, row_num) = self._get_user_row(old_user)
        if row:
            row.itemData[self.name_col] = new_user
            if old_user == self.local_user.name:  # we have to keep our 'self' name current for muting
                self.local_user.name = new_user
            self._row_updated(row_num)
            logging.info("UserModel: change_user_name: succeeded")
            if str(old_user) == self.gm_name:
                self.gm_name = new_user
            if str(old_user) == self.server_name:
                self.server_name = new_user
            return True
        logging.warn("UserModel: change_user_name: failed")
        return False


    def change_user_color(self, user_name, new_color):
        logging.info("UserModel: change_user_color: " + str(user_name) + ", " + str(new_color))
        (row, row_num) = self._get_user_row(user_name)
        if row:
            if isinstance(new_color, str):
                new_color = QColor(new_color)
            row.bgColor = new_color
            row.fontColor = self.get_font_color(new_color)
            self._row_updated(row_num)
            logging.info("UserModel: change_user_color: succeeded")
            return True
        logging.warn("UserModel: change_user_color: failed")
        return False


    def get_font_color(self, bg_color):
        return QColor(nd_helpers.separate_vals(self.loud_color, bg_color))


    def set_gm(self, new_gm_name):
        old_gm_name = self.get_gm_name()
        if old_gm_name == new_gm_name:
            return True

        (row, row_num) = self._get_user_row(new_gm_name)
        if not row:
            logging.info("UserList.set_gm: didn't find username")
            return False

        (old_row, old_row_num) = self._get_user_row(old_gm_name)
        # if not old_row:
        #     logging.info("UserList.set_gm: didn't find old GM")
        #     return False

        row.setData(self.gm_col, True)
        self.gm_name = new_gm_name
        new_index = self.index(row_num, self.gm_col)
        self.dataChanged.emit(new_index, new_index)

        if old_row:
            old_row.setData(self.gm_col, False)
            old_index = self.index(old_row_num, self.gm_col)
            self.dataChanged.emit(old_index, old_index)

        self.syncLocalGMPrivs[bool].emit(self.local_gm_privs())
        return True


    def remove_users(self, user_names):
        logging.info("UserModel: remove_users: " + str(user_names))
        success = True
        # Must only be a single item, because items only gets one pass
        for name in user_names:
            ary = ['*' for i in range(self.num_cols)]
            ary[0] = name
            pos = self.get_item_index(ary)  # have to send the item as an array, not a string
            # get_item_index returns a QModelIndex. We need a row number.
            if pos.isValid():
                try:
                    self.remove_row(pos.row())
                except:
                    try:
                        self.remove_row(pos)
                    except:
                        success = False

            return success
        logging.warn("UserModel: remove_users: unreachable?")
        return success


    def _user_name(self, index):
        return index.sibling(index.row(), self.name_col).data()


    def mute_user(self, user_index, mute=True):
        if self._user_name(user_index) == self.local_user.name:
            raise ValueError("Can't mute yourself.")
            return

        self.setData(user_index.sibling(user_index.row(), self.muted_col), mute, role=Qt.CheckStateRole)


    def _update_muted_color(self, user_index):
        user_item = self.getItem(user_index)
        if user_item.itemData[self.muted_col]:
            user_item.set_foreground(self.muted_color)
        else:
            user_item.set_foreground(QColor(0, 0, 0))
        self._row_updated(user_index.row())


    def is_muted(self, user_index):
        if isinstance(user_index, str):
            row = self._get_user_row(user_index)
            user_index = self.index(row, self.name_col)
        return user_index.sibling(user_index.row(), self.muted_col).data()




class UserSort(TreeSort):
    # Implementing sort filters seems the best way to sort data without modifying it
    def __init__(self, source_model=None, parent=None):
        TreeSort.__init__(self, source_model, parent)
        self.muted_col = self.sourceModel().muted_col
        self.gm_col = self.sourceModel().gm_col


    def lessThan(self, left, right):
        if left.column() == self.muted_col:
            return left.data(role=Qt.CheckStateRole) < right.data(role=Qt.CheckStateRole)

        if left.column() == self.gm_col and self.sourceModel().local_gm_privs():
            return left.data(role=Qt.UserRole) < right.data(role=Qt.UserRole)

        return left.data() < right.data()

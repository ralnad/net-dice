import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QAbstractItemView, QMenu, QTreeView, QAction
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QAbstractItemView, QMenu, QTreeView, QAction


# The base Tree view class

class TreeView(QTreeView):
    def __init__(self, parent=None):
        QTreeView.__init__(self, parent)

        QTreeView.setSortingEnabled(self, True)
        QTreeView.allColumnsShowFocus = True
        QTreeView.setSelectionMode(self, QAbstractItemView.ExtendedSelection)

        # this is by far the easiest way to create a right-click menu for an object.
        self.header().setContextMenuPolicy(Qt.CustomContextMenu)
        self.header().customContextMenuRequested.connect(self.headerContextMenu)


    def minimize_cols(self):
        for col in range(self.model().columnCount()):
            self.resizeColumnToContents(col)


    def setModel(self, model):
        QTreeView.setModel(self, model)
        self.minimize_cols()


    def _get_selected_list(self, pos):
        # this should get all the selected rows, or the row under the mouse if none are selected
        selected = self.selectedIndexes()
        under = self.indexAt(pos)
        if under not in selected:
            return [under]
        return selected


    def headerContextMenu(self, pos):
        # this function has two purposes:
        # 1: show how to make a right-click menu
        # 2: show how to hide and show columns
        globalPos = self.mapToGlobal(pos)  # QPoint
        # globalPos is helpful if you wanted to know where on screen the menu action was triggered.
        menu = QMenu()
        model = self.model()
        header = self.header()
        for col in range(model.columnCount()):
            visible_index = header.logicalIndex(col)
            # makes the cols display in the right order if user moves them
            action_name = str(model.headerData(visible_index, Qt.Horizontal, Qt.DisplayRole))
            new_action = QAction(action_name, self)
            new_action.setCheckable(True)
            new_action.setChecked(not header.isSectionHidden(visible_index))
            if visible_index == 0:
                # don't want to get rid of the parenting column
                new_action.setEnabled(False)
            new_action.setData(visible_index)  # used to determine column below
            menu.addAction(new_action)

        menu.addSeparator()
        # another way to pipe clicks is by just calling a function.
        # This helper function makes the action for you.
        menu.addAction("&Minimize Columns", self.minimize_cols)

        # now, actually show the menu and capture the clicked item
        selectedItem = menu.exec_(globalPos)  # QAction
        # use menu.popup instead of menu.exec_ if you want a non-blocking menu

        if selectedItem:
            '''
            use this if you want something to happen if anything is selected
            or, in this case, deal with the item that is selected
            more specifically, the data associated with it
            far easier than making custom signals, slots, and functions for it all
            '''
            column = selectedItem.data()
            if column:
                if header.isSectionHidden(column):
                    header.showSection(column)
                    self.minimize_cols()
                else:
                    header.hideSection(column)
        else:
            # or execute something if nothing is selected
            logging.warn("headerContextMenu: nothing")
            print("headerContextMenu: nothing")

import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt, QModelIndex
else:
    from PySide.QtCore import Signal, Qt, QModelIndex

from dice_roller import DiceRoller
from .groups_tree import GroupModel

from ui import drag_drop as dd
from ui.ui_mods import ui_helpers
import net_dice_helpers as nd_helpers




# The widget list model, derived from a list
# We hardcode the headers in.
#
# The list uses the following style storage model:
# [folder name], "" -- for a folder, as a derivative of the tree
#                      this means that this item in the list has children
# [widget name], [roll value] -- for a widget, as a derivative of the tree
#                      means that this item in the list has zero children
#
# Searching involves get_item_index passing an array with all the search
# terms
# Note that: "*" in a column is a wild card search
class WidgetModel(GroupModel):
    showMessage = Signal(str)

    def __init__(self, item_colors=False, parent=None):
        self.header = ['Widget Name', 'Widget Roll']
        GroupModel.__init__(self, self.header, editable=True, item_colors=item_colors, parent=parent)

        # self.statusBar = statusBar
        self.roller = DiceRoller()


    # Prevent bad/duplicate names
    def check_widget_name(self, name, curr_index=None):
        name = nd_helpers.validate_roll_name(name)
        if not name or name == 'roll':
            raise SyntaxError("Empty or Invalid Widget Name")

        # if curr_index and curr_index.isValid(): # Should be a QModelIndex
        found = self.get_item_index([name, '*'])
        if found.isValid() and ((not curr_index) or curr_index.row() != found.row()):
            raise ValueError("Duplicate Widget Name " + name)

        return name


    # Make sure a widget roll edit is a valid roll
    def check_widget_roll(self, roll):
        logging.info("Checking Widget Roll")
        try:
            self.roller.evalRoll(roll, self.get_widget_dict(flatten=True))
            return True
        except:
            raise


    # In the event we change the folder behaviour
    def is_folder(self, item):
        return item.child_count() > 0


    # Designed to set a cell of data given an index (row)
    def setData(self, index, value, role=Qt.EditRole):
        if role != Qt.EditRole:
            return False

        item = self.getItem(index)
        col = index.column()
        old_value = str(index.data())
        try:
            if col == 0:
                # make sure name is unique and valid
                value = self.check_widget_name(value, index)
            else:  # it's column 1 (we only have 2 columns)
                if self.is_folder(item):
                    # self.statusBar.showMessage("Error: Groups can't also be rolls")
                    raise TypeError("Error: Groups can't also be rolls")
                    return False
                self.check_widget_roll(str(value))
        except Exception as err:
            # Note in the event of an exception, we want to bring back
            # the fail edit so the user can correct it
            # TODO put that code here
            # self.statusBar.showMessage(str(err))
            self.showMessage[str].emit(str(err))
            # raise err
            return False
        result = item.setData(col, str(value))

        new_value = str(value)

        if old_value != new_value:
            if self.item_exists(['*', '[{0}]'.format(old_value)], substring_ok=True):
                title = "Update Dependencies?"
                message = "One or more widgets depends on this widget name. Would you like these dependencies updated?"
                update_refs = ui_helpers.generic_ok_dialog(title, message)
                if update_refs:
                    reps = self.substring_replace('[{old}]', '[{new}]'.format(old=old_value, new=new_value))
                    # self.statusBar.showMessage("Dependencies changed: "+str(reps))
                    self.showMessage[str].emit(str(reps))

        if result:
            self.dataChanged.emit(index, index)
        return result


    # QMimeData *DragDropListModel::mimeData(const QModelIndexList &indexes) const
    def mimeData(self, indexes):
        mimeData = dd.DiceMimeData()

        # see ui/drag_drop for rolls format
        rolls_list = []  # the rolls we want to roll
        subrolls_dict = {}  # the necessary subrolls to roll the above

        for index in indexes:
            if index.isValid() and index.column() == 0:
                name = str(index.data())
                roll = str(index.sibling(index.row(), 1).data())
                if roll is None or roll == '':
                    continue

                parent_index = index.parent()
                if parent_index.column() != 0:
                    parent_index = parent_index.sibling(parent_index.row(), 0)

                rolls_list.append({"Roll Name": name, "Roll": roll, "Row": index.row(), "Parent": str(parent_index.data())})

                subrolls_dict.update(self.roller.getNeededWidgets(roll, self.get_widget_dict(flatten=True)))
                # a low-overhead way of combining dictionaries

        mimeData.setRolls(rolls_list)
        mimeData.setSubRolls(subrolls_dict)
        mimeData.setTextFromRolls()

        return mimeData


    # Optimized get and set. We know we only have 2 columns, so we don't need a list of data.
    def get_widget_dict(self, flatten=False):
        return self._widget_dict_helper(GroupModel.get_group_dict(self), flatten)


    # Recursively go through the list and create a dictionary object for
    # exporting
    def _widget_dict_helper(self, group_dict, flatten=False):
        new_dict = {}
        for item in group_dict:
            if isinstance(group_dict[item], dict):
                if flatten:
                    temp_dict = self._widget_dict_helper(group_dict[item], flatten)
                    for thing in temp_dict:
                        new_dict[thing] = temp_dict[thing]
                else:
                    new_dict[item] = self._widget_dict_helper(group_dict[item])
            else:
                if len(group_dict[item]) == 1:
                    new_dict[item] = group_dict[item][0]
        return new_dict


    # Used to blank the widget list then add everything from a dictionary
    def set_from_widget_dict(self, wid_dict):
        self.clear_rows()
        return self.add_to_list_from_dict(wid_dict)


    # The behavior of the tree is just a flat list, so we have to simulate
    # a dictionary object
    #
    # duplicates behaviour is added as safety for importing widgets on top
    # existing widgets
    #     None | Overwrite | Ignore
    #
    # overwrite uses recursion in the event that you are importing a folder
    #
    # in the event that the widget is added, update the rows' child count
    def add_to_list_from_dict(self, group_dict, duplicates_behavior=None, parent=QModelIndex()):
        # duplicates_behavior defaults to None, can be 'Overwrite' or 'Ignore'
        for key in group_dict:
            if duplicates_behavior == "Ignore":
                if self.item_exists([key, '*'], parent=parent):
                    continue
            elif duplicates_behavior == "Overwrite":
                existing_index = self.get_item_index([key, '*'], parent)
                if existing_index.isValid():
                    existing_item = self.getItem(existing_index)
                    new_row = [key]
                    if isinstance(group_dict[key], dict):
                        new_row.append('')
                        if not self.add_to_list_from_dict(group_dict[key], duplicates_behavior, existing_index):
                            return False
                    else:
                        new_row.append(group_dict[key])
                    existing_item.setAllData(new_row)
                    continue

            if isinstance(group_dict[key], dict):
                success = self.add_item([str(key), ''], parent=parent)
                if success:
                    row = self.index(self.getItem(parent).child_count() - 1, 0, parent)
                    if not self.add_to_list_from_dict(group_dict[key], duplicates_behavior, row):
                        return False
                else:
                    logging.info("add_to_list_from_dict: Big fail")
            else:
                if not self.add_item([key, group_dict[key]], parent=parent):
                    return False
        return True

import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
else:
    from PySide.QtCore import Qt

from .list_tree import ListModel




class EmailModel(ListModel):
    def __init__(self, parent=None):
        self.header=['Player Name', 'Player E-Mail Address', 'Enabled']
        ListModel.__init__(self, self.header, editable=True, item_colors=False, parent=parent)
        self.check_col = 2


    def data(self, index, role):
        if not index.isValid():
            return None

        item = index.internalPointer()

        if index.column() == self.check_col:
            if role == Qt.CheckStateRole:
                return int( Qt.Checked if item.itemData[index.column()] else Qt.Unchecked )
            elif role == Qt.DisplayRole:
                return ''

        return ListModel.data(self, index, role)


    def flags(self, index):
        if not index.isValid():
            return None

        if index.column() == self.check_col:
            flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable
        else:
            flags = ListModel.flags(self, index)

        return flags


    def setData(self, index, value, role=Qt.EditRole):
        if index.column() == self.check_col:
            if role == Qt.EditRole:
                return False
            if role == Qt.CheckStateRole:
                item = self.getItem(index)
                item.itemData[index.column()] = value
                self.dataChanged.emit(index, index)
                return True

        return ListModel.setData(self, index, value, role)


    def remove_emails(self, email_names):
        logging.info("EmailModel: remove_emails: " + str(email_names))
        success = True
        for name in email_names:
            if name.column() == 0:
                try:
                    self.remove_row(name.row())
                except:
                    try:
                        self.remove_row(name)
                    except:
                        success = False
        return success


    def enable_emails(self, email_names, new_state=True):
        logging.info("EmailModel: enable_emails: " + str(email_names))
        success = True
        for name in email_names:
            if name.column() == self.check_col:
                if not self.setData(name, new_state, Qt.CheckStateRole):
                    success = False
        return success


    def get_email_dict(self, sender_info=None):
        # Optimized get and set. We have to treat the checkbox specially.
        # we assume all emails are unique, so they're good to use as keys.
        email_dict = {}
        for row_num in range(self.rootItem.child_count()):
            row_data = self.rootItem.child(row_num).all_data()
            email = str(row_data[1])
            email_dict[email] = {str(self.header[n]):str(row_data[n]) for n in range(len(self.header))}
            email_dict[email][str(self.header[self.check_col])] = self.rootItem.child(row_num).data(self.check_col, Qt.CheckStateRole)
            del(email_dict[email][self.header[1]])
        return email_dict


    def set_emails_from_dict(self, email_dict):
        for email in email_dict:
            email_list = ['' for i in range(len(self.header))]
            for col in range(len(self.header)):
                if self.header[col] in email_dict[email]:
                    email_list[col] = email_dict[email][self.header[col]]
            email_list[1] = email
            checked = True if email_list[self.check_col] else False
            email_list[self.check_col] = ''
            self.add_item(email_list)
            new_index = self.get_item_index(email_list)
            data = new_index.internalPointer()
            data.setData(self.check_col, checked)

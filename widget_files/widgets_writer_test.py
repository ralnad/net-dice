import pickle, json, os, re

'''pf_dict = {
'Attributes':{
    'STR':'10', 'DEX':'10', 'INT':'10', 'WIS':'10', 'CHA':'10', 'CON':'10',
    'STR_MOD':'([STR]-10)/2', 'DEX_MOD':'([DEX]-10)/2',
    'INT_MOD':'([INT]-10)/2', 'WIS_MOD':'([WIS]-10)/2',
    'CHA_MOD':'([CHA]-10)/2', 'CON_MOD':'([CON]-10)/2'},
  'Initiative':'1d20+[DEX_MOD]', 'Level':'1', 'Caster Level':'[Level]',
  'Saves':{
    'Fortitude Save':'1d20+[CON_MOD]+0+0',
    'Reflex Save':'1d20+[DEX_MOD]+0+0',
    'Will Save':'1d20+[WIS_MOD]+0+0'},
  'BAB':'0',
  'Melee To Hit':'1d20+[BAB]+[STR_MOD]+0+0',
  'Ranged To Hit':'1d20+[BAB]+[DEX_MOD]+0+0',
  'Melee Hits':'[Melee To Hit]-(0,5,10)',
  'Ranged Hits':'[Ranged To Hit]-(0,5,10)',
  'Sword Damage':'1d4-1',
  'Bow Damage':'1d4-1',
  'Melee Damage':'[Sword Damage]+[STR_MOD]',
  'Ranged Damage':'[Bow Damage]+[STR_MOD]',
  'Skills':{
    'Intimidate':'d20+[CHA_MOD]+0+0',
    'Heal':'d20+[WIS_MOD]+0+0',
    'Knowledge':'d20+[INT_MOD]+0+0'
  }
}

with open('pathfinder.json', 'w') as outfile:
    json.dump(pf_dict, outfile, indent=4)'''

base_path = "../../safe/"
#end_path = "widget_files/"
widgets_filename = re.compile(r"widgets_.*\.txt")
widgets_files = [x for x in os.listdir(base_path) if widgets_filename.search(x)]
print(widgets_files)

for name in widgets_files:
    print("processing", name)
    f = open(os.path.join(base_path, name), 'r')
    file_dict = pickle.load(f)

    with open(name[:-4]+'.json', 'w') as outfile:
        json.dump(file_dict, outfile, indent=4)

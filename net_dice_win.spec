# -*- mode: python -*-

block_cipher = None

app_base = r'C:\Users\dumbor\Documents\net-dice'

qt5dir = r'C:\Python\Python35-32\Lib\site-packages\PyQt5\Qt\bin'
icon_name = r'ui\icons\NetDice.ico'

a = Analysis(['net_dice.py'],
             pathex=[app_base, qt5dir],
             binaries=None,
             datas=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
          cipher=block_cipher)

a.datas += [('help_text.html', os.path.join(app_base, r'ui\help_text.html'), 'DATA'),
            ('change_log.txt', os.path.join(app_base, r'ui\change_log.txt'), 'DATA'),
            ('cacert.pem', os.path.join(app_base, r'network\cacert.pem'), 'DATA'),
            ('NetDice.ico', os.path.join(app_base, icon_name), 'DATA'),
           ]

widgets = 'widget_files'
a.datas.extend([(file, os.path.join(app_base, widgets, file), 'DATA') for file in os.listdir(widgets) if os.path.isfile(os.path.join(widgets, file)) and file.endswith('.json')])

# onedir commands
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='NetDice',
          debug=False,
          strip=False,
          upx=False,
          console=False,
          icon=os.path.join(app_base, icon_name),
          manifest=os.path.join(app_base, 'netdice_manifest.xml')
          )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='NetDice')

# onefile commands
# exe = EXE(pyz,
#           a.scripts,
#           a.binaries,
#           a.zipfiles,
#           a.datas,
#           name='NetDice',
#           debug=False,
#           strip=False,
#           upx=True,
#           console=False,
#           icon=os.path.join(app_base, icon_name),
#           manifest=os.path.join(app_base, 'netdice_manifest.xml')
#           )

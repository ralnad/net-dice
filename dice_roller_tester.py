#!/usr/local/bin/python3
from dice_roller import DiceRoller, N0, T1, T2, H1, H2

def main():
	roller = DiceRoller(N0)
	show, bad, oddb, dice, nums, equa = [1, 0, 1, 0, 0, 0]

	good_rolls = []
	if oddb:
		good_rolls = good_rolls + [
			"3.0/2.0",
			"1.0",
			"1 + 0",
			"0--1",
			"2 + 2/3",
			"1 + 1.5 * 3",

			"5+(-1,-1)",
			"5+(-1, -1)",
			"5+( -1, -1)",
			"((1, 2) + 4, 3) + 10",
			"(1,2) + (3,4)",
			"1+((2),(3))+4",
			"1 + (2, 3) + 4",
			"1 + (2, 3) + 4",

			"(d%)",
			"1D20",
			"1.1d20",
			"1d20.1",
			"1.1d20.1",
			"1d20 * (1.5)",
			"1d20*0.667",
			"2d1.5",
			"4d6 * 1.5",
			"d%",
			"1d%",
			"1.0d%",
			"d%crit55",
			"d%fail44",
			"d20.0",
			"dd",
			"d",
			"5d6.1ch3",
			"5d6.1cl3",

			"1d20crit1fail20",
			"1d20chcrit1",
			"1d20crit5fail15",
			"1d20.1crit5",
			"1d20.1fail15",
			"1 + d20crit1 + 1",
			"1 + d20crit1 + 1, 1+d20fail20",
			"2d20chcrit1",
			"d20crit1+1",
			"1d20rege",
			"floor(1.5) * 3",
			"floor(1 + 0.5)",
			"max(d1fail1,-1)",
			"max(d20crit1, 25)",
			"min(d20crit1, d20)",
			"floor(1.1)",

			"1d20crit15>10",
			"[Foo]",
			"[Bar]>d20",
			"[a b]",
			"[a - b]",
			"-[a - b]",
			"(1+1)d(1+1)",
			"1+(d)",
			"1d6ex1",

			"ceil(1.1)",
			"ceil(1.0)",
			"ceil(0.9)",
			"ceil(-0.9)",
		]

	# Dice testing
	if dice:
		test_digits = [str(x) for x in range(1, 7)]
		for x in test_digits:
			for y in test_digits:
				for z in test_digits:
					for p in ["crit", "fail", "ch", "cl", "rege", "rele", "ex"]:
						if p in ["crit", "fail"] and y <= z:
							continue
						good_rolls.append(x + "d" + y + p + z)


	sp = lambda x, y: " " if x == y else ""
	pa = lambda x, y: "(" + y + ")" if x else y
	neg = lambda x: "-" if x else ""
	pt = lambda x: "." if x else ""

	# Pure number evaluation
	if nums:
		test_digits = [str(x) for x in range(0, 11)]
		for p3 in [False, True]:
			for left in test_digits:
				for right in test_digits:
					for n in [False, True]:
						for p in [False, True]:
							for s in range(0, 6):
								good_rolls.append(pa(p3, sp(1, s) + neg(n) + sp(2, s) + left + pt(p) + right + sp(3, s)))

	# Simple equation testing
	if equa:
		test_numbers = ["-1", "0", "1", "-1.1", "0.1", "1.1"]
		test_ops = ["<", "<=", ">", ">=", "==", "!=", "+", "-", "*", "/", "%"]
		for p3 in [False, True]:
			for left in test_numbers:
				for right in test_numbers:
					for op in test_ops:
						if op not in ["/", "%"] and int(float(right)) != 0:
							for s in range(0, 5):
								for p1 in [False, True]:
									for p2 in [False, True]:
										good_rolls.append(pa(p3, sp(1, s) + pa(p1, left) + sp(2, s) + op + sp(3, s) + pa(p2, right) + sp(4, s)))

	for a in good_rolls:
		try:
			result = roller.evalRoll(a, {"Foo": "[Bar]", "Bar": "10", "a b": "1", "a - b": "2"})
			if roller.validate(result):
				if show:
					print("Expected succeed:", "{:15}".format(" ".join([str(b["value"]) for b in result[-1]])), "<=", a)
			else:
				print("Unexpected fail:", a)
		except Exception as ex:
			print("Unexpected crash:", a, ex)

	if bad:
		bad_rolls = [
			"(1).1",
			"(.)",
			"1.1.",
			".",
			"1.",
			".1",
			"-",
			"1 + .",
			". + 1",
		]
		for a in bad_rolls:
			try:
				result = roller.evalRoll(a)
				for b in result:
					print("???", b)
			except Exception as ex:
				if show:
					print("Expected fail:", "{!s:55}".format(ex), "<=", a)

if __name__ == "__main__":
	main()

#!/bin/bash

python unit_tests.py fresh
python unit_tests.py
# python unit_tests.py pyside  # disabled for now, pyside's qwait doesn't seem to work
python unit_tests.py server_start &
sleep 5
python unit_tests.py client

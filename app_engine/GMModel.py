#!/usr/bin/env python
from google.appengine.ext import ndb

"""
DataModel to track the current GM for a session
"""
class GMModel(ndb.Model):
	# Guid of the user that is the GM
	"""
	Unique ID for the user that is designated as the GM
	"""
	guid = ndb.StringProperty()

	"""
	State variable for associating the GM with a session
	"""
	session = ndb.StringProperty()

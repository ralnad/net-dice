import ast
import dice_roller
import json
import webapp2
from google.appengine.ext import ndb

from CommonFunctions import *
from WSWidgets import *


class WSHandleRolls(webapp2.RequestHandler):
	def post(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)

	def get(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		#print ConvertToDiceRollerWidgets()
		widget = ndb.gql("SELECT * FROM WidgetModel WHERE name = :1", query).get()
		if widget:
			results = dice_roller.EvalRoll(widget.equation, ConvertToDiceRollerWidgets())
		else:
			results = dice_roller.EvalRoll(query, ConvertToDiceRollerWidgets())
		self.response.write(json.dumps(results))
		#old = dice_roller.ConvertToOldStyle(results)
		#self.response.write(json.dumps(old))
	
	def put(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)

	def delete(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)


def ParametersToWidgets(parameters):
	# Note that this requires the incoming "dict" to be entirely
	# made of strings, consider if there's a way handle numbers
	# in the roller code or convert everything to strings already
	if len(parameters):
		return ast.literal_eval(parameters)
	return {}
	

class WSHandleRollsOneOff(webapp2.RequestHandler):
	def _try_decorator(func):
		def w(self):
			try:
				func(self)
			except Exception as e:
				self.response.write(e.message)
				self.response.set_status(500)
		return w

	@_try_decorator
	def post(self):
		self.response.headers['Content-Type'] = 'application/json'

		session = self.request.params['session']
		roll_name = self.request.params['roll name']
		white = self.request.params['white']
		black = self.request.params['black']
		name = self.request.params['name']
		hide = self.request.params['hide'] == 'true'
		results = dice_roller.EvalRoll(self.request.params['equation'], ParametersToWidgets(self.request.params['widgets']))
		AppendLog(session, white, black, name, json.dumps({'User Info': {'Hide Rolls': hide, 'Name': name}, 'Roll': results, 'Roll Name': roll_name}))
		self.response.write(json.dumps({"Result": results}))

	@_try_decorator
	def get(self):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)
	
	@_try_decorator
	def put(self):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)

	@_try_decorator
	def delete(self):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(404)

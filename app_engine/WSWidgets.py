import webapp2
import json
import copy
from google.appengine.ext import ndb


from WidgetModel import *


def WSFilter(items, query="*"):
	if query != "*":
		query = query.split(",")
		# deep copy in the event mutable objects are passed around
		return [copy.deepcopy(a) for a in items if a["name"] in query]
	return items


class WSHandleWidgets(webapp2.RequestHandler):
	def post(self):
		self.response.headers['Content-Type'] = 'application/json'

		name = self.request.get('name')
		equation = self.request.get('equation')

		widget = WidgetModel(name=name, equation=equation)
		widget.put()
		self.response.write(json.dumps(SerializeToDict(widget)))

	def get(self):
		self.response.headers['Content-Type'] = 'application/json'

		widgets = FetchWidgets(self.request.get('query', default_value="*"))
		if widgets:
			results = []
			for a in widgets:
				results.append(SerializeToDict(a))
			self.response.write(json.dumps(results))
		else:
			self.abort(404)

	def put(self):
		self.response.headers['Content-Type'] = 'application/json'

		equation = self.request.get('equation')

		widgets = FetchWidgets(self.request.get('query', default_value="*"))
		if widgets:
			results = []
			for a in widgets:
				a.equation = equation
				a.put()
				results.append(SerializeToDict(a))
			self.response.write(json.dumps(results))
		else:
			self.abort(404)
	
	def delete(self):
		self.response.headers['Content-Type'] = 'application/json'

		widgets = FetchWidgets(self.request.get('query', default_value="*"))
		if widgets:
			for a in widgets:
				a.key.delete()
		else:
			self.abort(404)


def ConvertToDiceRollerWidgets():
	return {a.name: a.equation for a in FetchWidgets("*")}


def FetchWidgets(query):
	widgets = WidgetModel.query()
	if query != "*":
		query = query.split(',')

		for a in query:
			p1, p2 = a.split("=")
			widgets = widgets.filter(WidgetModel._properties[p1] == p2)
	widgets = widgets.fetch()
	return widgets


def SerializeToDict(item):
	result = item.to_dict()
	result["key"] = item.key.id()
	return result


class WSHandleWidget(webapp2.RequestHandler):
	def post(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		self.abort(500)

	def get(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		widget = ndb.gql("SELECT * FROM WidgetModel WHERE name = :1", query).get()
		if widget:
			self.response.write(json.dumps(SerializeToDict(widget)))
		else:
			self.abort(404)
	
	def put(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		widget = ndb.gql("SELECT * FROM WidgetModel WHERE name = :1", query).get()
		if widget:
			widget.equation = self.request.get('equation')
			widget.put()
			self.response.write(json.dumps(SerializeToDict(widget)))
		else:
			self.abort(404)

	def delete(self, query):
		self.response.headers['Content-Type'] = 'application/json'
		widget = ndb.gql("SELECT * FROM WidgetModel WHERE name = :1", query).get()
		if widget:
			widget.key.delete()
		else:
			self.abort(404)
	



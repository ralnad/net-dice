#!/usr/bin/env python
from google.appengine.ext import ndb

"""
DataModel for storing a widget
"""
class WidgetModel(ndb.Model):
	"""
	State variable for storing the reference name for the widget, will be
	verified if it's unique
	"""
	name = ndb.StringProperty()

	"""
	State variable for storing the equation for the widget
	"""
	equation = ndb.StringProperty()

	"""
	State variable for tracking who's the owner of the widget
	"""
	owner = ndb.StringProperty()

	"""
	State variable for tagging the widget
	"""
	tag = ndb.StringProperty()

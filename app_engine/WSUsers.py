import json
import logging
from google.appengine.ext import ndb

from CounterModel import CounterModel
from GMModel import *
from UserModel import *
from CommonFunctions import *

from WSHandler import WSHandlerUsers_Base

"""
Handler for a collection of users in a session
"""
class WSHandleUsers(WSHandlerUsers_Base):
	"""
	Check to see if the connecting user already exists
	Else, check to see if the existing session has a GM and if not make the current user GM
	Add the new user
	"""
	@WSHandlerUsers_Base._try_decorator
	def post(self):
		out = self.__setup__()
		logging.info("WSHandleUsers.post: out={0}".format(out))

		if False and None != GetQuery(self._key_user(out), "SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["guid"], out["session"])):
			logging.info("WSHandleUsers.post: user already exists")
			self._writejson(self._dev_msg("user already exists"))
		else:
			is_new_gm = not self._query_gm(out)
			if is_new_gm:
				logging.info("WSHandlerUsers_Base.post: new gm added")
				PutQuery(self._key_gm(out), GMModel(guid=out["guid"], session=out["session"]))

			proposed_names = self._SetName(out, out["name"])
			new_user = UserModel(guid=out["guid"], name=proposed_names, session=out["session"], color=out["color"])
			PutQuery(self._key_user(out), new_user)			
			SetMemcache(self._key_users(out), None)
			logging.info("WSHandlerUsers_Base.post: new_user={0}".format(new_user))

			AppendLog(out["session"], "*", "", proposed_names, json.dumps({"New User": proposed_names}))

			counter_model = GetQuery(self._key_counter_model(out), "SELECT * FROM CounterModel WHERE session = :1", (out["session"],))
			log_marker = 0
			if counter_model:
				log_marker = counter_model.counter
			logging.info("WSHandlerUsers_Base.post: counter_model={0}".format(counter_model))
			logging.info("WSHandlerUsers_Base.post: log_marker={0}".format(log_marker))
			self._writejson({"dev_message": "user {0} added".format(proposed_names), "name": proposed_names, "log_marker": log_marker, "Is GM": is_new_gm})

	"""
	Get the possible GM, current user, and list of users
	If the current user is GM, grab the GUIDS and user names
	If the current user is not GM, grab only the user names
	"""
	@WSHandlerUsers_Base._try_decorator
	def get(self):
		out = self.__setup__()
		[gm, user, users] = self._get_gm_users(out)

		if users:
			if gm and user and self._cmp_usermodels(user, gm):
				out_ = [{"Is GM": a.guid == gm.guid, "Name": a.name, "Color": a.color, "Guid": a.guid} for a in users]
			else:
				out_ = [{"Is GM": a.guid == gm.guid, "Name": a.name, "Color": a.color} for a in users]
			AppendLog(out["session"], user.guid, "", "system", json.dumps(out_))
			self._writejson(self._dev_msg(out_))
		else:
			self._errorOut("Cannot verify you should be able to see the list", 401)
	
	"""
	Not really any GM specific options for this one, except kicking which
	is covered by delete
	"""
	@WSHandlerUsers_Base._try_decorator
	def put(self):
		self._errorOut("Cannot update all users", 404)

	"""
	Get the possible GM, current user, and list of users
	If the current user is GM, kick everyone and remove the GM
	"""
	@WSHandlerUsers_Base._try_decorator
	def delete(self):
		out = self.__setup__()
		[gm, user, users] = self._get_gm_users(out)

		if gm and user and self._cmp_usermodels(user, gm):
			num_users = len(users)
			for user_ in users:
				PutQuery("user_".format(user_.guid), None)
				user_.key.delete()
			PutQuery(self._key_gm(out), None)
			gm.key.delete()
			SetMemcache(self._key_users(out), None)

			AppendLog(out["session"], "*", "", user.name, json.dumps({"message": "emptied the session"}))

			self._writejson(self._dev_msg("Removed {0} users".format(num_users)))
		elif (gm and user and user.guid != gm.guid) or not gm:
			self._writejson(self._dev_msg("Requires GM permissions"))
		else:
			self._writejson(self._dev_msg("Session is already empty"))


"""
Handler for a specific user
"""
class WSHandleUser(WSHandlerUsers_Base):
	"""
	Do nothing
	"""
	@WSHandlerUsers_Base._try_decorator
	def post(self, query):
		self._errorOut("Cannot create a user with this function, instead use POST with /ws/v1/users.json", 404)

	"""
	Get information about the specified user
	"""
	@WSHandlerUsers_Base._try_decorator
	def get(self, query):
		self._errorOut("Cannot get information for the specified user", 404)

	"""
	Update the name for a given user, GM has the ability to force rename people
	"""
	@WSHandlerUsers_Base._try_decorator
	def put(self, query):
		out = self.__setup__()
		[gm, user] = self._get_gm_and_user(out)
		to_rename_user = self._get_query_user(query)

		if gm and user and to_rename_user and (self._is_gm(gm, user) or self._cmp_usermodels(to_rename_user, user)):
			out_ = {}
			if out["name"] != "":
				old_name, to_rename_user.name = to_rename_user.name, self._SetName(out, out["name"])
				out_["New User"] = to_rename_user.name
				out_["Old User Name"] = old_name
			if out["color"] != "":
				old_name = to_rename_user.name
				old_color, to_rename_user.color = to_rename_user.color, out["color"]
				out_["Old Color"] = old_color
				out_["New Color"] = to_rename_user.color
			PutQuery(self._key_user(out), to_rename_user)
			SetMemcache(self._key_users(out), None)
			AppendLog(out["session"], "*", "", old_name, json.dumps(out_))

			self._writejson(out_)
		else:
			self._errorOut(self._dev_msg("User not authorized"), 401)

	"""
	User can leave (kick himself) or GM can force kick a player
	If the GM leaves reassign the GM if possible
	"""
	@WSHandlerUsers_Base._try_decorator
	def delete(self, query):
		out = self.__setup__()
		[gm, user] = self._get_gm_and_user(out)
		to_kick_user = self._get_query_user(query)
		to_del = []

		if to_kick_user and (self._is_gm(gm, user) or (user and self._cmp_usermodels(to_kick_user, user))):
			if self._is_gm(gm, user) and self._cmp_usermodels(gm, to_kick_user):
				new_gm = ndb.gql("SELECT * FROM UserModel WHERE guid != :1 AND session = :2", query, out["session"]).get()
				if new_gm:
					gm.guid = new_gm.guid
					PutQuery(self._key_gm(out), gm)
					AppendLog(out["session"], new_gm.name, "", "system", json.dumps({"dev_message": "You are the new GM", "Is GM": True}))
				else:
					SetMemcache(self._key_gm(out), None)
					to_del.append(gm.key)

			to_kick_name = to_kick_user.name
			AppendLog(out["session"], "*", "", "system", json.dumps({"Disconnected User": to_kick_name}))
			AppendLog(out["session"], query, "", "system", json.dumps(self._dev_msg("user not present")))

			to_del.append(to_kick_user.key)
			SetMemcache(self._key_users(out), None)
			SetMemcache("user_{0}".format(query), None)

			ndb.delete_multi(to_del)
			self._writejson(self._dev_msg("Kicking user {0}".format(to_kick_name)))
		else:
			self._errorOut(self._dev_msg("User not authorized"), 401)

import json
import time
import traceback
import webapp2

from CommonFunctions import *
from CounterModel import *
from LogModel import *
from UserModel import *


"""
Handler for web service polling that will act as the primary dump point for
getting information out to the user
"""
class WSCommLink(webapp2.RequestHandler):
	def __setup__(self):
		self.response.headers['Content-Type'] = 'application/json'

		"""
		An experiment to help make things more maintainable
		might be a performance penalty for polling many unused parameters
		"""
		out = {}
		for a in [ ("guid", ""), ("name", ""), ("session", "default"), ("is_from", ""), ("white", "*"), ("black", ""), ("message", ""), ("log_marker", "-1")]:
			if a[0] in self.request.params:
				out[a[0]] = self.request.params[a[0]]
			else:
				out[a[0]] = a[1]
		return out

	def _try_decorator(func):
		def w(self):
			try:
				func(self)
			except Exception as e:
				logging.info(e.message)
				logging.info(traceback.format_exc())
				self.response.write(e.message)
				self.response.set_status(500)
		return w
	
	"""
	Disable for this handler
	"""
	@_try_decorator
	def post(self):
		self.response.write("Post has been disabled the CommLink")
		self.response.set_status(404)

	"""
	Primary method of sending information to the user
	NOTE: very much a hack since channels are the preferred method of handling
	real time communication, but channels are too gimped on AppEngine
	"""
	@_try_decorator
	def get(self):
		out = self.__setup__()

		sleep_time = 0.15
		start = time.time()
		stop = time.time()
		val = None
		is_from = None
		log_marker = None

		"""
		Because it's long polling, you do need to return at some point. All
		the client's can wait as long as they need to, but App Engine has it's
		own timeout set. So we'll respond regardless of anything happened at
		66% of what the default timing is
		"""
		user = GetQuery("user_" + str(out["guid"]), "SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["guid"], out["session"],))
		while stop - start < 20 and user:
			user = GetQuery("user_" + str(out["guid"]), "SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["guid"], out["session"],))
			try:
				log_marker = int(out["log_marker"])
			except:
				logging.info("Reseting log_marker %s" % out)
				log_marker = -1
			counter_model = GetQuery("counter_model_" + str(out["session"]), "SELECT * FROM CounterModel WHERE session = :1", (out["session"],))
			if not user:
				self.response.write(json.dumps({"dev_message": "user not present"}))
				return

			if not counter_model:
				log_marker = 0
				counter_model = CounterModel(counter=0, session=out["session"])
				PutQuery("counter_model_" + str(out["session"]), counter_model)

			if user and log_marker >= 0 and log_marker < counter_model.counter:
				log = GetQuery("log_" + str(out["session"]) + "_" + str(log_marker + 1), "SELECT * FROM LogModel WHERE session = :1 AND counter = :2", (out["session"], log_marker + 1,))
				if log and log_marker == log.counter - 1:
					white_ = log.white.split(";")
					black_ = log.black.split(";")
					if (log.white == "*" or (user.guid in white_) or (user.name in white_)) and ((user.name not in black_) or (user.guid not in black_)):
						val = log.message
						is_from = log.is_from
					else:
						val = json.dumps({"dev_message": "not intended recipient"})
						is_from = "system"
					log_marker += 1
					break
				else:
					time.sleep(sleep_time)
			else:
				time.sleep(sleep_time)
			stop = time.time()

		if val == None:
			package = {"Is From": "system", "log_marker": log_marker, "Code": 200, "Result": {"dev_message": "too slow"}}
			self.response.write(json.dumps(package))
		else:
			# val is already in JSON
			dict_val = json.loads(val)
			package = {"Is From": is_from, "log_marker": log_marker, "Code": 200, "Result": dict_val}
			self.response.write(json.dumps(package))
			
	"""
	Update all the users with some piece of information
	"""
	@_try_decorator
	def put(self):
		out = self.__setup__()
		AppendLog(out["session"], out["white"], out["black"], out["is_from"], out["message"])
		self.response.write(json.dumps({"message": "message received"}))

	"""
	Disable for this handler
	"""
	@_try_decorator
	def delete(self):
		self.response.write("Delete has been disabled for CommLink")
		self.response.set_status(404)

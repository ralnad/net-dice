import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QObject, pyqtSignal as Signal
    from PyQt5.QtWidgets import QActionGroup
else:
    from PySide.QtCore import QObject, Signal
    from PySide.QtGui import QActionGroup


from dice_roller import DiceRoller
from trees_and_lists.dice_tree_model import DiceModel
from trees_and_lists.widget_list import WidgetList
from trees_and_lists.email_list import EmailList

import net_dice_helpers as nd_helpers
import net_dice_globals as NDG




# Roller modes
NONE = 0
ROLL_TYPING = 1
ROLL_BUILDING = 2

VALID_ROLLER_MODES = [ROLL_TYPING]




# Handles the rolling interface: roll typing, widgets, and chat window.
class RollerUI(QObject):
    sendRoll = Signal(dict)

    def __init__(self, UI, prefs_dict={}):
        QObject.__init__(self)
        self.ui = UI.ui
        self.main_win = UI  # for changing the window UI size
        self.connection_mode = None

        self.roller = DiceRoller()  # rolls the rolls given to it
        self.roll_tree = DiceModel(self.ui)  # the chat window
        self.roll_widgets = WidgetList(self.ui)  # the widget list

        self.email_list = EmailList(self.ui)

        self.roll_mode = NONE  # builder has been deprecated, should only be typer
        self.auto_make_widgets = False

        self._setup_connections()

        self.set_roller_info(prefs_dict)


    def _setup_connections(self):
        connGroup = QActionGroup(self.ui.menuModes)
        connGroup.addAction(self.ui.actionRoll_Typing)
        self.ui.actionRoll_Typing.setChecked(True)

        connGroup2 = QActionGroup(self.ui.menuRolls)
        connGroup2.addAction(self.ui.actionAll_Rolls)
        connGroup2.addAction(self.ui.actionMultiRolls_Only)
        connGroup2.addAction(self.ui.actionNo_Rolls)
        self.ui.actionMultiRolls_Only.setChecked(True)

        self.ui.actionRoll_Typing.triggered.connect(self.setup_typing_mode)

        self.ui.chatTreeView.setModel(self.roll_tree)
        self.ui.widgetView.giveUI(self.ui)
        # self.ui.widgetView.setModel(self.roll_widgets)

        self.ui.rollButton.clicked.connect(self.roll_dice)
        self.ui.makeWidgetButton.clicked.connect(self.make_roll_widget)
        self.ui.clearHistoryButton.clicked.connect(self.clear_roll_hist)
        self.ui.broadcastButton.clicked.connect(self.send_chat)

        self.ui.actionAll_Rolls.triggered.connect(self.ui.chatTreeView.expand_all_rolls)
        self.ui.actionMultiRolls_Only.triggered.connect(self.ui.chatTreeView.expand_multi_rolls)
        self.ui.actionNo_Rolls.triggered.connect(self.ui.chatTreeView.expand_no_rolls)
        self.ui.actionAuto_Make_Roll_Widgets.toggled.connect(self.set_auto_make_widgets)

        self.ui.rollNameEdit.set_roll_edit_box(self.ui.rollEdit)


    def set_auto_make_widgets(self, make_em):
        logging.info("Toggling auto creating widgets to: " + make_em)
        self.auto_make_widgets = make_em

        self.ui.makeWidgetButton.setEnabled(not self.auto_make_widgets)


    def set_conn_mode(self, conn_mode):
        logging.info("Roller_UI: Setting the connection mode: " + conn_mode)
        self.connection_mode = conn_mode
        self.roll_widgets.set_conn_mode(conn_mode)


    def get_roller_info(self):
        logging.info("Gathering the roller information")
        prefs_dict = {
            'Widget Prefs': self.roll_widgets.get_widget_info(),
            'Roll Mode': self.roll_mode,
            'Auto-Make Roll Widgets': self.auto_make_widgets,
            'Expand Rolls Level': self.ui.chatTreeView.get_expand_rolls(),
            'Dice Tree Prefs': self.roll_tree.get_prefs(),
            'E-Mail List Prefs': self.email_list.get_email_dict()
        }
        return prefs_dict


    def set_roller_info(self, prefs_dict):
        logging.info("Acting on the settings from the loaded preferences")
        temp_UI = ROLL_TYPING

        if 'Widget Prefs' in prefs_dict:
            self.roll_widgets.set_widget_info(prefs_dict['Widget Prefs'])

        # if 'Roll Mode' in prefs_dict:
        #    temp_UI = prefs_dict['Roll Mode']

        if 'Dice Tree Prefs' in prefs_dict:
            self.roll_tree.set_prefs(prefs_dict['Dice Tree Prefs'])

        if 'Auto-Make Roll Widgets' in prefs_dict:
            self.ui.actionAuto_Make_Roll_Widgets.setChecked(prefs_dict['Auto-Make Roll Widgets'])

        if 'Expand Rolls Level' in prefs_dict:
            self.ui.chatTreeView.set_expand_rolls(prefs_dict['Expand Rolls Level'])
            level = prefs_dict['Expand Rolls Level']
            if level == 2:
                self.ui.actionAll_Rolls.setChecked(True)
            elif level == 1:
                self.ui.actionMultiRolls_Only.setChecked(True)
            else:
                self.ui.actionNo_Rolls.setChecked(True)

        if 'E-Mail List Prefs' in prefs_dict:
            self.email_list.set_emails_from_dict(prefs_dict['E-Mail List Prefs'])

        self.set_roll_mode(temp_UI)


    def save_widgets(self):
        logging.info("Saving the widgets")
        self.roll_widgets.save_widgets()



    # --- Roll Modes ---
    def set_roll_mode(self, mode):
        logging.info("Setting the roller mode: " + str(mode))
        self.setup_typing_mode()


    # Switches the UI to roll typing mode.
    # Window resizing doesn't work as desired.
    def setup_typing_mode(self):
        logging.info("Setting the typing widget builder")
        self.ui.actionRoll_Typing.setChecked(True)
        if self.roll_mode != ROLL_TYPING:
            prev_state = self.roll_mode
            self.roll_mode = ROLL_TYPING
            self.ui.builderBox.hide()
            self.ui.rollsBox.show()
            if prev_state == ROLL_BUILDING:
                # self.ui.builderBox.setMinimumWidth(0)
                # self.ui.builderBox.resize(0, self.ui.builderBox.height())
                self.main_win.resize(self.main_win.width() - self.ui.builderBox.width(), self.main_win.height())


    def setup_name(self, roll_txt):
        logging.info("Grabbing the new widget's name")
        return nd_helpers.validate_roll_name(roll_txt)



    # --- Chat ---
    def send_chat(self):
        logging.info("Responding to things being entered into the chat")
        self.ui.statusBar.clearMessage()
        chat_txt = str(self.ui.broadcastEdit.text()).strip()
        if chat_txt:
            if self.connection_mode not in NDG.ONLINE_STATES:
                self.chat_received({'Chat': chat_txt})
            else:
                self.sendRoll[dict].emit({'Chat': chat_txt})
        self.ui.broadcastEdit.setText('')


    def chat_received(self, chat_dict):
        logging.info("Responding to things being sent to the chat log")
        chat_txt = chat_dict['Chat']
        user_dict = chat_dict['User Info'] if 'User Info' in chat_dict else {}
        self.roll_tree.add_broadcast(chat_txt, user_dict)


    def _ui_roll(self, roll_str):
        try:
            logging.info(roll_str)
            result = self.roller.evalRoll(roll_str, self.roll_widgets.get_flat_widget_dict())
            if not result:
                raise Exception("setup_roll failed; roller returned no reason")
            return result
        except Exception as e:
            logging.info(str(e))
            raise


    # --- Roll Typing ---
    def _get_typed_roll(self):
        return str(self.ui.rollEdit.text()).strip()


    def setup_typed_roll(self):
        logging.info("Validating the roll")
        self.ui.statusBar.clearMessage()

        roll_str = self._get_typed_roll()
        try:
            result = self._ui_roll(roll_str)
        except Exception as e:
            self.ui.rollStatusLabel.setText(str(e))
            return False

        self.ui.rollStatusLabel.setText('')
        return result


    def get_typed_roll_widgets(self):
        logging.info("Gathering the widgets for the selected roll")
        roll_str = self._get_typed_roll()

        try:
            return self.roller.getNeededWidgets(roll_str, self.roll_widgets.get_flat_widget_dict())
        except:
            return False


    def setup_typed_name(self):
        name = str(self.ui.rollNameEdit.text())
        logging.info("Get the roll's name: " + name)
        return self.setup_name(name)


    def roll_dice(self):
        logging.info("Rolling the dice and routing appropriately")
        self.ui.statusBar.clearMessage()

        roll_name = self.setup_typed_name()
        if not roll_name:
            return False

        result = self.setup_typed_roll()
        if not result:
            return False

        if self.connection_mode not in NDG.ONLINE_STATES:
            # result['Roll Name'] = roll_name
            txt = self.roll_received({'Roll': result, 'Roll Name': roll_name})
            self.ui.rollStatusLabel.setText(txt)
        else:
            roll_str = self._get_typed_roll()
            send_widgets = self.get_typed_roll_widgets()
            self.sendRoll[dict].emit({'Roll': roll_str, 'Roll Name': roll_name, 'Roll Widgets': send_widgets})

        if self.auto_make_widgets:
            self.make_roll_widget()


    def roll_received(self, roll_dict):
        logging.info("Capture a roll and prepare it for displaying")
        # generates a txt string for the status bar or elsewhere with roll info
        user_info = roll_dict['User Info'] if 'User Info' in roll_dict else {}
        logging.info("roll_received: " + str(roll_dict))
        return "Roll Result: " + self.roll_tree.add_rolls(roll_dict['Roll'], roll_dict['Roll Name'], user_info)


    def make_roll_widget(self):
        logging.info("Adding a widget to the list")
        if self.setup_typed_roll():
            roll = self.setup_name(self.ui.rollEdit.text())
            roll_name = str(self.ui.rollNameEdit.text()).strip()
            added = self.roll_widgets.addWidget(roll_name, roll)
            return added


    def clear_roll_hist(self):
        logging.info("Clearing out the history")
        self.roll_tree.clear_rows()

import os, sys
import unittest
from ui import sip_setup

QAPP = None
APP = None

ROLL_WAIT = 90

SHORT_NAME = 'r'
LONG_NAME = 'very long typed roll name goes here'

# The dice roller is object based
# this function takes the objects and converts them to the equivalent strings
class NetDiceTest(unittest.TestCase):
    # def setUp(self):
        # a reminder in case we need to set something up later


    def runVariantTest(self):
        with self.assertRaises(ImportError):
            run_app(variant='')


    def runStartClientTest(self):
        # should change us to server mode
        QTest.keyPress(APP.ui.centralwidget, Qt.Key_C, Qt.AltModifier, delay=10)

        APP.ui.userNameEdit.setText('')
        APP.ui.clientConnectToEdit.setText('')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        self.assertStatusBar('Invalid server IP')
        self.checkOnline(online=False)

        APP.ui.clientConnectToEdit.setText('localhost')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        QTest.qWait(10)
        self.assertStatusBar('Server IP set')
        QTest.qWait(2000)
        self.assertStatusBar('Added')

        self.checkOnline(online=True)

        self.assertTrue(APP.ui.clientLayout.isVisible())
        self.assertFalse(APP.ui.serverLayout.isVisible())
        self.assertFalse(APP.ui.kickUserButton.isVisible())
        self.assertFalse(APP.ui.emailBox.isVisible())

        user_model = APP.ui.usersView.base_model
        self.assertEqual(user_model.rowCount(), 2)


    def runLocalTest(self):
        # should change us to local mode
        APP.ui.actionLocal_Mode.trigger()

        # checks if we're in an online-capable state
        self.assertFalse(APP.ui.userBox.isVisible())
        self.assertFalse(APP.ui.usersView.isVisible())

        self.assertFalse(APP.ui.emailBox.isVisible())


    def runEmailStartTest(self):
        # should change us to email mode
        APP.ui.actionE_Mail_Mode.trigger()

        self.assertTrue(APP.ui.userBox.isVisible())
        self.assertFalse(APP.ui.serverLayout.isVisible())
        self.assertFalse(APP.ui.clientLayout.isVisible())
        self.assertFalse(APP.ui.connectLayout.isVisible())
        self.assertTrue(APP.ui.emailBox.isVisible())
        self.assertFalse(APP.ui.usersView.isVisible())


    def runEmailSendTest(self):
        email_model = APP.ui.emailView.model().sourceModel()
        email_base = APP.roller.email_list

        widget_view = APP.ui.widgetView
        widget_model = widget_view.model().sourceModel()

        self.resetWidgets()
        self.clearTextTest()

        APP.ui.emailSenderEdit.setText('')

        APP.ui.emailView.selectAll()
        all = APP.ui.emailView.selectedIndexes()
        email_model.remove_emails(all)
        QTest.qWait(2000)

        self.assertEqual(email_model.rowCount(), 0)
        QTest.mouseClick(APP.ui.sendButton, Qt.LeftButton, delay=5)
        # self.assertRaisesRegexp(IndexError, "Can't send an empty E-Mail", email_base.send_email)
        self.assertStatusBar("Can't send an empty E-Mail.")

        base_char = widget_model.get_item_index(['Base Character', '*'])
        widget_view.expand(widget_view.model().mapFromSource(base_char))
        self.setWidgetSelection(widget_model.get_item_index(['Initiative', '*']), widget_model.get_item_index(['Base Bonus', '*']))
        QTest.mouseClick(APP.ui.rollSelectedButton, Qt.LeftButton, delay=5)

        QTest.mouseClick(APP.ui.sendButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT * 4)
        # self.assertRaisesRegexp(IndexError, "Must provide a sending email address", email_base.send_email)
        self.assertStatusBar("Must provide a sending email address.")

        APP.ui.emailSenderEdit.setText('netdiceproject')
        QTest.qWait(ROLL_WAIT * 4)
        QTest.mouseClick(APP.ui.sendButton, Qt.LeftButton, delay=5)
        # self.assertRaisesRegexp(IndexError, "Must send an email to at least one recipient", email_base.send_email)
        self.assertStatusBar("Must send an email to at least one recipient.")

        # can't get this working right now. Ideally we'd test item creation directly.
        # QTest.mouseClick(APP.ui.emailView, Qt.LeftButton, delay=5)
        # QTest.qWait(5)
        new_item = ['roger', 'netdiceproject@gmail.com', '']
        self.assertTrue(email_model.add_item(new_item))
        new_index = email_model.get_item_index(new_item)
        email_model.setData(new_index.sibling(new_index.row(), 2), 2, role=Qt.CheckStateRole)

        self.assertEqual(new_index, email_model.index(0, 0))
        self.assertEqual(new_index.data(), 'roger')

        QTimer.singleShot(250, self.emailDialog)
        email_base.send_email()
        QTest.qWait(3000)
        self.assertStatusBar("Username and Password not accepted")

        # Uncomment this to manually test the actual email sending. Requires password.
        '''
        email_base.send_email()
        QTest.qWait(3000)
        self.assertStatusBar("Email Sent Successfully")
        '''


    def runStartServerTest(self):
        # should change us to server mode
        QTest.keyPress(APP.ui.centralwidget, Qt.Key_S, Qt.AltModifier, delay=10)

        APP.ui.userNameEdit.setText('')
        APP.ui.serverPortEdit.setText('')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        self.assertStatusBar('Invalid Server Port')
        self.checkOnline(online=False)

        APP.ui.serverPortEdit.setText('20')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        self.assertStatusBar('Invalid Server Port')
        self.checkOnline(online=False)

        APP.ui.serverPortEdit.setText('30000')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        self.assertStatusBar('Invalid Server Port')
        self.checkOnline(online=False)

        APP.ui.serverPortEdit.setText('1234')
        QTest.mouseClick(APP.ui.userConnectButton, Qt.LeftButton, delay=10)
        QTest.qWait(2500)

        user_model = APP.ui.usersView.base_model

        self.assertTrue(APP.ui.serverLayout.isVisible())
        self.assertFalse(APP.ui.clientLayout.isVisible())
        self.assertTrue(APP.ui.kickUserButton.isVisible())
        self.assertFalse(APP.ui.emailBox.isVisible())
        self.assertEqual(user_model.rowCount(), 1)

        self.checkOnline(online=True)


    def rollFromTypedTest(self):
        # chat_model = APP.ui.chatTreeView.model()
        # widget_model = APP.ui.widgetView.model().sourceModel()

        # roll a long-name typed roll
        APP.ui.rollNameEdit.setText(LONG_NAME)
        APP.ui.rollEdit.setText("3d20")
        QTest.mouseClick(APP.ui.rollButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertLastChatLine("3d20", child=True)

        # roll a short-name typed roll
        APP.ui.rollNameEdit.setText(SHORT_NAME)
        QTest.keyPress(APP.ui.rollEdit, Qt.Key_Return, delay=5)
        QTest.qWait(ROLL_WAIT * 20)
        self.assertLastChatLine("3d20")


    def rollWithWidgetsTest(self):
        widget_model = APP.ui.widgetView.model().sourceModel()

        # make a widget
        widget_model.clear_rows()
        QTest.mouseClick(APP.ui.makeWidgetButton, Qt.LeftButton, delay=5)
        self.assertEqual(str(widget_model.index(0, 0).data()), SHORT_NAME)
        self.assertEqual(str(widget_model.index(0, 1).data()), '3d20')

        # trying to make the widget again results in a 'duplicate widget' error
        self.assertRaises(ValueError, APP.roller.make_roll_widget)

        # roll the created widget
        APP.ui.widgetView.selectAll()
        QTest.mouseClick(APP.ui.rollSelectedButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertLastChatLine("3d20")

        # add the created widget to another roll
        APP.ui.widgetView.selectAll()
        APP.ui.rollNameEdit.setText(LONG_NAME)
        APP.ui.rollEdit.setText("3d20")
        QTest.mouseClick(APP.ui.addToRollButton, Qt.LeftButton, delay=5)
        QTest.keyPress(APP.ui.rollEdit, Qt.Key_Return, delay=10)
        QTest.qWait(ROLL_WAIT)
        self.assertLastChatLine(', ')
        self.assertLastChatLine("[{0}], 3d20".format(SHORT_NAME), child=True)


    def runWidgetsTest(self):
        chat_model = APP.ui.chatTreeView.model()
        widget_view = APP.ui.widgetView
        widget_model = widget_view.model().sourceModel()

        self.resetWidgets()

        base_char = widget_model.get_item_index(['Base Character', '*'])
        widget_view.expand(widget_view.model().mapFromSource(base_char))
        # print base_char, base_char.child(0,0), base_char.child(6,0)
        # print base_char.row(), base_char.child(0,0).row(), base_char.child(6,0).row()
        # print widget_model.get_item_index(['Initiative', '*'])
        self.setWidgetSelection(widget_model.get_item_index(['Initiative', '*']), widget_model.get_item_index(['Base Bonus', '*']))
        self.assertEqual(len(widget_view.selectedIndexes()), 14)  # 7 rows, 2 columns

        QTest.mouseClick(APP.ui.groupButton, Qt.LeftButton)
        QTest.qWait(200)
        self.groupingDialog()
        group_index = widget_model.get_item_index(['test group', '*'])
        self.assertTrue(group_index.isValid())

        self.clearTextTest()
        self.setWidgetSelection(group_index, group_index)
        QTest.keyPress(widget_view, Qt.Key_R, delay=10)
        QTest.qWait(ROLL_WAIT)
        self.assertStatusBar("No Widgets could be rolled")
        self.assertEqual(chat_model.rowCount(), 0)

        widget_view.expand(widget_view.model().mapFromSource(group_index))
        QTest.qWait(ROLL_WAIT)
        first_kid = group_index.child(0, 0)
        num_kids = widget_model.rowCount(group_index)
        self.assertEqual(num_kids, 7)
        QTest.qWait(ROLL_WAIT * 2)

        last_kid = group_index.child(2, 0)
        self.setWidgetSelection(first_kid, last_kid)
        QTest.qWait(500)
        QTest.mouseClick(APP.ui.rollSelectedButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertEqual(chat_model.rowCount(), 3)
        QTest.qWait(ROLL_WAIT * 2)
        # add something for widget editing


    def broadcastTextTest(self):
        APP.ui.broadcastEdit.setText("broadcastText")
        QTest.mouseClick(APP.ui.broadcastButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertLastChatLine("broadcastText")

        APP.ui.broadcastEdit.setText("broadcastMoreText")
        QTest.keyPress(APP.ui.broadcastEdit, Qt.Key_Return, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertLastChatLine("broadcastMoreText")
        self.clearTextTest()


    def clearTextTest(self):
        # clear the chat window
        chat_model = APP.ui.chatTreeView.model()

        QTest.mouseClick(APP.ui.clearHistoryButton, Qt.LeftButton)
        QTest.qWait(ROLL_WAIT)
        self.assertEqual(chat_model.rowCount(), 0)


    def dragDropTest(self):
        chat_model = APP.ui.chatTreeView.model()
        widget_view = APP.ui.widgetView
        widget_model = widget_view.model().sourceModel()

        self.resetWidgets()

        # drag widgets->chat window
        self.clearTextTest()
        base_char = widget_model.get_item_index(['Base Character', '*'])
        widget_view.expand(widget_view.model().mapFromSource(base_char))
        self.setWidgetSelection(widget_model.get_item_index(['Initiative', '*']), widget_model.get_item_index(['Base Bonus', '*']))

        indexes = APP.ui.widgetView.selectedIndexes()
        mime_data = widget_model.mimeData(indexes)

        self.dropOnto(APP.ui.chatTreeView, mime_data)
        QTest.qWait(ROLL_WAIT * 4)
        self.assertGreater(chat_model.rowCount(), 6)

        # drag widgets->broadcast edit line
        self.clearTextTest()
        self.dropOnto(APP.ui.broadcastEdit, mime_data)
        QTest.keyPress(APP.ui.broadcastEdit, Qt.Key_Return, delay=5)
        QTest.qWait(ROLL_WAIT)
        self.assertEqual(chat_model.rowCount(), 1)

        # drag widgets->roll edit
        self.clearTextTest()
        APP.ui.rollEdit.setText('')
        self.dropOnto(APP.ui.rollEdit, mime_data)
        QTest.mouseClick(APP.ui.rollButton, Qt.LeftButton, delay=5)
        QTest.qWait(ROLL_WAIT * 4)
        self.assertEqual(chat_model.rowCount(), 1)

        # drag widgets->roll name edit
        self.clearTextTest()
        self.dropOnto(APP.ui.rollNameEdit, mime_data)
        QTest.keyPress(APP.ui.rollNameEdit, Qt.Key_Return, delay=5)
        QTest.qWait(ROLL_WAIT * 4)
        self.assertEqual(chat_model.rowCount(), 1)

        # drag widgets->roll name edit
        self.clearTextTest()
        APP.ui.rollNameEdit.setText(LONG_NAME)
        APP.ui.rollNameEdit.selectAll()
        mime_data = QMimeData()
        mime_data.setText(APP.ui.rollNameEdit.text())
        self.dropOnto(APP.ui.chatTreeView, mime_data)
        QTest.qWait(ROLL_WAIT)
        self.assertEqual(chat_model.rowCount(), 1)
        self.assertLastChatLine(LONG_NAME)

        # drag widgets->roll name edit
        self.clearTextTest()
        APP.ui.rollEdit.setText("3d20+45, d20crit15fail5, 16d%")
        APP.ui.rollEdit.selectAll()
        mime_data.setText(APP.ui.rollEdit.text())
        self.dropOnto(APP.ui.chatTreeView, mime_data)
        QTest.qWait(ROLL_WAIT)
        self.assertEqual(chat_model.rowCount(), 1)
        self.assertLastChatLine("3d20+45, d20crit15fail5, 16d%")


    # --- helper functions for tests ---


    def checkOnline(self, online=True):
        # checks if we're in an online-capable state
        self.assertTrue(APP.ui.userBox.isVisible())
        self.assertTrue(APP.ui.userBox.isEnabled())
        self.assertTrue(APP.ui.usersView.isVisible())

        if online:
            self.assertTrue(APP.ui.usersView.isEnabled())
        else:
            self.assertFalse(APP.ui.usersView.isEnabled())


    def resetWidgets(self):
        widget_model = APP.ui.widgetView.model().sourceModel()
        widget_model.clear_rows()

        QTimer.singleShot(150, self.actOnResetDialog)
        APP.ui.actionRevert_to_Default_Widgets.trigger()
        self.assertGreater(widget_model.rowCount(), 0)
        self.assertEqual(widget_model.rowCount(), 7)


    def dropOnto(self, widget, mime_data):
        action = Qt.CopyAction | Qt.MoveAction
        pt = widget.rect().center()
        drag_drop = QDropEvent(pt, action, mime_data, Qt.LeftButton, Qt.NoModifier)
        drag_drop.acceptProposedAction()
        widget.dropEvent(drag_drop)


    def printIndexInfo(self, ind):
        print(ind, ind.data(), ind.row(), ind.column(), ind.parent().data(), ind.isValid())


    def setWidgetSelection(self, start_ind, end_ind):
        widget_view = APP.ui.widgetView
        start_index = widget_view.model().mapFromSource(start_ind)
        end_index = widget_view.model().mapFromSource(end_ind)
        select = QItemSelection(start_index, end_index)

        widget_view.clearSelection()
        select_rect = widget_view.visualRegionForSelection(select).boundingRect()
        widget_view.setSelection(select_rect, QItemSelectionModel.Select | QItemSelectionModel.Rows)


    def actOnDialog(self, text):
        for widget in QAPP.focusWidget().parent().children():
            if isinstance(widget, QDialogButtonBox):
                for button in widget.buttons():
                    if str(button.text()) == text:
                        QTest.mouseClick(button, Qt.LeftButton)


    def standardDialog(self):
        self.actOnDialog("OK")


    def emailDialog(self):
        self.actOnDialog("Send")


    def groupingDialog(self):
        for widget in QAPP.focusWidget().parent().children():
            # print(widget.objectName())
            if isinstance(widget, QLineEdit):
                widget.setText("test group")
                break
        self.standardDialog()


    def actOnResetDialog(self):
        self.actOnMsgBox("Reset Widgets")


    def actOnMsgBox(self, text):
        main_wid = APP.ui.centralwidget
        reset_button = None
        for child in main_wid.children():
            if isinstance(child, QMessageBox):
                for button in child.buttons():
                    if str(button.text()) == text:
                        reset_button = button
                        break
            if reset_button:
                break

        if reset_button:
            QTest.mouseClick(reset_button, Qt.LeftButton, delay=10)
            return

        assert False


    def assertLastChatLine(self, text, child=False):
        chat_model = APP.ui.chatTreeView.model()
        parent = chat_model.index(chat_model.rowCount() - 1, 0)
        if child:
            child = chat_model.index(0, 0, parent)
            self.assertEqual(str(child.data()), text)
        else:
            if text not in str(parent.data()):
                print("assertLastChatLine", str(parent.data()))
                assert False


    def assertStatusBar(self, text):
        if text not in str(APP.ui.statusBar.currentMessage()):
            print("assertStatusBar", str(APP.ui.statusBar.currentMessage()))
            assert False




for_each_mode_tests = ["rollFromTypedTest", "rollWithWidgetsTest", "broadcastTextTest", "runWidgetsTest", "dragDropTest"]
# email_mode_tests = ["runEmailStartTest", "runEmailSendTest"]
email_mode_tests = []  # disable email mode tests until email mode works with oauth


def mainSuite():
    tests = ["runVariantTest", "runLocalTest"] + for_each_mode_tests + ["runStartServerTest"] + for_each_mode_tests + email_mode_tests + for_each_mode_tests
    return unittest.TestSuite(list(map(NetDiceTest, tests)))


def serverSuite():
    tests = ["runStartServerTest"] + for_each_mode_tests
    return unittest.TestSuite(list(map(NetDiceTest, tests)))


def clientSuite():
    tests = ["runStartClientTest"] + for_each_mode_tests
    return unittest.TestSuite(list(map(NetDiceTest, tests)))


def specialSuite():
    # tests = ['runStartServerTest', 'rollFromTypedTest', 'rollWithWidgetsTest']
    tests = ['runLocalTest', 'runWidgetsTest']
    return unittest.TestSuite(list(map(NetDiceTest, tests)))


def clear_files():
    try:
        os.remove("prefs.txt")
        os.remove("widgets.txt")
        os.remove("widgets.json")
        os.remove("Logger.txt")
        os.remove("*.pkl")
    except:
        pass




if __name__ == "__main__":
    fresh = False
    if 'fresh' in sys.argv:
        clear_files()
        fresh = True

    if 'server_start' in sys.argv:
        print('server')
        tests = serverSuite()
    elif 'client' in sys.argv:
        print('client')
        tests = clientSuite()
    elif 'special' in sys.argv:
        print('special')
        tests = specialSuite()
    else:
        print('main')
        tests = mainSuite()

    if 'pyside' in sys.argv:
        '''from PySide.QtGui import QDropEvent, QMessageBox, QDialogButtonBox, QItemSelection, QItemSelectionModel, QLineEdit
        from PySide.QtTest import QTest
        from PySide.QtCore import Qt, QMimeData, QTimer

        from net_dice import run_app

        (QAPP, APP) = run_app(variant='PySide', testing_mode=True)'''
        print('PySide testing disabled for now')
    else:
        sip_setup.SIPsetup()

        from PyQt5.QtGui import QDropEvent
        from PyQt5.QtWidgets import QMessageBox, QDialogButtonBox, QLineEdit
        from PyQt5.QtCore import Qt, QMimeData, QTimer, QItemSelection, QItemSelectionModel
        from PyQt5.QtTest import QTest

        from net_dice import run_app

        (QAPP, APP) = run_app(variant='PyQt', testing_mode=True)

    result = unittest.TestResult()
    unittest.TextTestRunner(verbosity=2).run(tests)
    if 'server_start' in sys.argv:
        # gives the client time to connect and run tests
        QTest.qWait(25000)
